module.exports = {
    rendering: {
        renderAnimationFirstAndThenEventList: {
            serverResponse1: {
                data: [{
                    _id: 'testId1',
                    category: "comment",
                    content: [
                        { format: 'plainText', text: 'this is a test' },
                        { format: 'html', text: '<p> this is a test </p>' }
                    ],
                    createdAt: '2018-01-01T00:01:00.000Z',
                    creationDate: '2018-01-01T00:00:00.000Z',
                    datasetId: null,
                    file: [],
                    fileSize: null,
                    filename: null,
                    investigationId: 'testInvestigationId',
                    machine: null,
                    previousVersionEvent: null,
                    software: null,
                    tag: [],
                    title: 'test title',
                    type: "annotation",
                    updatedAt: "2018-01-01T00:00:00.000Z",
                    username: "mchaille"
                }]
            },
            serverResponse2: { data: '1' },
        }
    }
}