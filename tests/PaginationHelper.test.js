import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { convertPageToPageEventIndexes, isRangeAvailableOnTheClient } from '../src/helpers/PaginationHelper'

describe('PaginationHelper tests', () => {
    describe("convertPageToPageEventIndex", () => {
        it("returns the proper page index", () => {
            let actualPage = 1;
            let actualEventsPerPage = 3;
            let actualFoundEventCount = 13;
            let expectedIndex = {
                start: 0,
                end: 2
            };
            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
            actualPage = 2;
            expectedIndex = {
                start: 3,
                end: 5
            };
            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
            actualPage = 3;
            expectedIndex = {
                start: 6,
                end: 8
            };
            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
            actualPage = 4;
            expectedIndex = {
                start: 9,
                end: 11
            };
            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
            actualPage = 5;
            expectedIndex = {
                start: 12,
                end: 12
            };
            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
        });

        it("returns the first page index when page is not provided", () => {
            let actualPage = null;
            let actualEventsPerPage = 3;
            let actualFoundEventCount = 13;
            let expectedIndex = {
                start: 0,
                end: actualEventsPerPage
            };

            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
        });

        it("returns the first page index when eventPerPage is not configured", () => {
            let actualPage = 2;
            let actualEventsPerPage = null;
            let actualFoundEventCount = 13;
            let expectedIndex = {
                start: 0,
                end: actualEventsPerPage
            };

            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
        });

        it("returns the first page index when the total number of event is not available", () => {
            let actualPage = 2;
            let actualEventsPerPage = 3;
            let actualFoundEventCount = null;
            let expectedIndex = {
                start: 0,
                end: actualEventsPerPage
            };

            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
        });

        it("returns the proper page index even if page is in string fromat", () => {
            let actualPage = "1";
            let actualEventsPerPage = 3;
            let actualFoundEventCount = 13;
            let expectedIndex = {
                start: 0,
                end: 2
            };
            expect(convertPageToPageEventIndexes(actualPage, actualEventsPerPage, actualFoundEventCount)).toEqual(expectedIndex);
        });
    })

    describe("isRangeAvailableOnTheClient", () => {
        describe("pageEventIndex is part of the downloadedEventIndex", () => {
            it("returns true; case 1", () => {
                let actualPageEventIndex = {
                    start: 2,
                    end: 5
                }
                let actualDownloadedEventIndex = {
                    start: 1,
                    end: 6
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(true);
            })

            it("returns true; case 2", () => {
                let actualPageEventIndex = {
                    start: 1,
                    end: 2
                }
                let actualDownloadedEventIndex = {
                    start: 1,
                    end: 6
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(true);
            })

            it("returns true; case 3", () => {
                let actualPageEventIndex = {
                    start: 2,
                    end: 6
                }
                let actualDownloadedEventIndex = {
                    start: 1,
                    end: 6
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(true);
            })

            it("returns true; case 4", () => {
                let actualPageEventIndex = {
                    start: 2,
                    end: 3
                }
                let actualDownloadedEventIndex = {
                    start: 2,
                    end: 3
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(true);
            })
        })

        describe("pageEventIndex is not part of the downloadedEventIndex", () => {
            it("returns false ; outside from the right side", () => {
                let actualPageEventIndex = {
                    start: 3,
                    end: 8
                }
                let actualDownloadedEventIndex = {
                    start: 1,
                    end: 6
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(false);
            })

            it("returns false; outside from the left side", () => {
                let actualPageEventIndex = {
                    start: 0,
                    end: 4
                }
                let actualDownloadedEventIndex = {
                    start: 1,
                    end: 6
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(false);
            })

            it("returns false; outside from both sides", () => {
                let actualPageEventIndex = {
                    start: 5,
                    end: 20
                }
                let actualDownloadedEventIndex = {
                    start: 4,
                    end: 10
                }

                expect(isRangeAvailableOnTheClient(actualPageEventIndex, actualDownloadedEventIndex)).toBe(false);
            })
        })

    })
})


