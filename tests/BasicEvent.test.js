import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { goodError1, goodError2, goodComment4 } from './GoodEventLibrary';
import { badComment3, badError1 } from './BadEventLibrary';
import BasicEvent from '../src/components/Event/BasicEvent';
import { LIST_VIEW } from '../src/constants/EventTypes';

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });

})

describe('BasicEvent', () => {
    function getWrapper(event) {
        return Enzyme.shallow(<BasicEvent
            event={event}
            toggleMode={() => null}
            user={{}}
            view={LIST_VIEW}
        />)
    }

    it('displays a fixed title when the event has no content', () => {
        let actualEvent = badError1;
        let myBasicEvent = getWrapper(actualEvent);
        let ExpectedText = "This event has no content";

        expect(myBasicEvent.find('p').text()).toEqual(ExpectedText);
    })

    describe('on annotation', () => {
        it('displays the last comment and icon of the annotation which has a previous version', () => {
            let actualEvent = goodComment4;
            let myBasicEvent = getWrapper(actualEvent);
            let expectedContent = [{
                format: 'plainText',
                text: 'new version of plain text'
            }, {
                format: 'html',
                text: '<p> new version of plain text </p>'
            }];
            expect(myBasicEvent.find("AnnotationContent")).not.toBeNull();
            expect(myBasicEvent.find("AnnotationContent").dive().find("Icon").dive().find('img').prop('src')).toBe('/comment.png');
            expect(myBasicEvent.find("AnnotationContent").dive().find('EventContentDisplayer').prop('content')).toEqual(expectedContent);
        })
    })

    describe('on notification', () => {
        it('displays the message and icon of the notification (no previous version)', () => {
            let actualEvent = goodError1;
            let myBasicEvent = getWrapper(actualEvent);
            let expectedContent = [{
                format: "plainText",
                text: "beamline lost"
            }];
            expect(myBasicEvent.find("NotificationContent")).not.toBeNull();
            expect(myBasicEvent.find("NotificationContent").dive().find('#divContainingHTMLNotificationInDocView').exists()).toBe(true);
            expect(myBasicEvent.find("NotificationContent").dive().find('Glyphicon').first().prop('glyph')).toBe('alert');
            expect(myBasicEvent.find("NotificationContent").dive().find('EventContentDisplayer').prop('content')).toEqual(expectedContent);
        })

        it('displays the event message and icon of the commented notification', () => {
            let actualEvent = goodError2;
            let myBasicEvent = getWrapper(actualEvent);
            let expectedContent = [{
                format: "plainText",
                text: "beamline lost"
            }]
            expect(myBasicEvent.find("NotificationContent")).not.toBeNull();
            expect(myBasicEvent.find("NotificationContent").dive().find('#divContainingHTMLNotificationInDocView').exists()).toBe(true);
            expect(myBasicEvent.find("NotificationContent").dive().find('Glyphicon').first().prop('glyph')).toBe('alert');
            expect(myBasicEvent.find("NotificationContent").dive().find('EventContentDisplayer').first().prop('content')).toEqual(expectedContent);
        })

        it('displays no comment and no icon for an uncommented notification ', () => {
            let actualEvent = goodError1;
            let myBasicEvent = getWrapper(actualEvent);

            expect(myBasicEvent.find("NotificationContent").dive().find({ src: '/comment.png' }).exists()).toBe(false);
            expect(myBasicEvent.find("NotificationContent").dive().find('EventContentDisplayer').length).toBe(1);
        })

        it('displays comment and comment icon for a commented notification ', () => {
            let actualEvent = goodError2;
            let myBasicEvent = getWrapper(actualEvent);
            let expectedCommentContent = actualEvent.content;

            expect(myBasicEvent.find("NotificationContent").dive().find('EventContentDisplayer').length).toBe(2);
            expect(myBasicEvent.find("NotificationContent").dive().find('EventContentDisplayer').at(1).prop('content')).toEqual(expectedCommentContent);
        })

    })

    describe("Event selection", () => {
        function getWrapper(event, isSelected) {
            return Enzyme.shallow(<BasicEvent
                event={event}
                isSelected={isSelected}
                toggleMode={() => null}
                user={{}}
                view={LIST_VIEW}
            />)
        }
        it("when event is selected the event is highlighted", () => {
            let actualEvent = goodComment4;
            let myBasicEvent = getWrapper(actualEvent, true);
            /* there must be a div which id is isSelected */
            expect(myBasicEvent.find("#selectedEvent")).not.toBeNull();
        })

        it("when event is not selected the event is not highlighted", () => {
            let actualEvent = goodComment4;
            let myBasicEvent = getWrapper(actualEvent, false);
            /* there must be a div which id is isSelected */
            expect(myBasicEvent.find("#selectedEvent").get(0)).toBeUndefined();
        })

        it("when isSelected prop is not provided the event is not highlighted", () => {
            let actualEvent = goodComment4;
            let myBasicEvent = getWrapper(actualEvent);
            /* there must be a div which id is isSelected */
            expect(myBasicEvent.find("#selectedEvent").get(0)).toBeUndefined();
        })
    })
})