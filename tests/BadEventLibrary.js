/**
 * This files hosts all events used in the tests. The 'bad' prefix indicates that this event is error free.
 * Content:
 * badComment1 : user comment, content array is empty, tag array is empty
 * badComment2 : user comment, content array does not contain the object hosting the plain text
 * badComment3 : user comment, tag array, category array, type do not exist
 * badComment4 : user comment, category is not in [error, comment, debug, commandLine, info]
 * badError1 : error with content undefined
 * badError2 : error which creation date is not set 
 * badNotification1: type exists but is not 'annotation' and not 'notification'
 * badNotification2: category is not set
 */

export const badComment1 = {
    title: 'event title 1',
    category: "comment",
    content: [],
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: [],
    type: "annotation",
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}

export const badComment2 = {
    title: 'event title 1',
    category: "comment",
    content: [{
        format: 'html',
        text: '<p> html text content 1 </p>'
    }],
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: [],
    type: "annotation",
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}

// there is no tag, no category
export const badComment3 = {
    title: 'event title 1',
    content: [],
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}

export const badComment4 = {
    title: 'event title 1',
    content: [],
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    type: "annotation",
    category: "unrecognized",
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}
export const badError1 = {
    title: 'event title 1',
    category: "error",
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: [],
    type: "notification",
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}

export const badError2 = {
    investigationId: 77570462,
    type: "notification",
    category: "error",
    username: "mchaille",
    software: "E-Dataportal",
    content: [{
        format: "plainText",
        text: "beamline lost"
    }]
}

export const badNotification1 = {
    title: 'event title 1',
    category: "comment",
    content: [],
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: [],
    type: "unknownType",
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}

export const badNotification2 = {
    title: 'event title 1',
    type: "notification",
    content: [],
    createdAt: "2018-09-10T13:23:08.727Z",
    creationDate: "2018-09-10T13:23:08.000Z",
    datasetId: null,
    file: [],
    fileSize: null,
    filename: null,
    investigationId: 77570462,
    machine: null,
    previousVersionEvent: null,
    software: null,
    tag: [],
    updatedAt: "2018-09-10T13:23:08.727Z",
    username: "mchaille"
}