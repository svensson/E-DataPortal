import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import EventFooter from '../src/components/Event/EventFooter'

import { Button } from 'react-bootstrap'

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });

})

describe('EventFooter Tests', () => {

    it('displays enabled button when prop isSaveButtonEnabled is true', () => {
        let myEventFooter = Enzyme.shallow(<EventFooter
            isSaveButtonEnabled={true}
            onSaveButtonClicked={() => null}
            onCancelButtonClicked={() => null} />)

        expect(myEventFooter.find(Button).first().prop('disabled')).toBe(false);
    })

    it('displays disabled button when prop isSaveButtonEnabled is false', () => {
        let myEventFooter = Enzyme.shallow(<EventFooter
            isSaveButtonEnabled={false}
            onSaveButtonClicked={() => null}
            onCancelButtonClicked={() => null} />)

        expect(myEventFooter.find(Button).first().prop('disabled')).toBe(true);
    })
})