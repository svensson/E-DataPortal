import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { badComment3, badNotification1 } from './BadEventLibrary';
import { LIST_VIEW } from '../src/constants/EventTypes';
import Event from '../src/components/Event/Event';

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });

})

describe('Event', () => {
    function getWrapper(event) {
        return Enzyme.shallow(<Event
            event={event}
            investigationId="testInvestigationId"
            reloadEvents={() => null}
            user={{}}
            view={LIST_VIEW}
        />)
    }

    it('displays nothing when the event type is not defined', () => {
        let actualEvent = badComment3;
        let myEvent = getWrapper(actualEvent);
        expect(myEvent.get(0)).toBe(null);
    })

    it('displays nothing when the event has an unknown type', () => {
        let actualEvent = badNotification1;
        let myEvent = getWrapper(actualEvent);

        expect(myEvent.get(0)).toBe(null);
    })
})