import { getUserSpecificSelectionFilters, getSelectionFiltersForMongoQuery, getSelectionFiltersBySearchCriteria } from '../src/containers/Event/SelectionFilterHelper';
require('it-each')({ testPerIteration: true });

const resources = require('./resources/selectionFilterHelper.resource');

describe("selectionFilterHelper", () => {
    describe('getUserSpecificSelectionFilters', () => {
        describe('handle errors', () => {
            it.each(resources.getUserSpecificSelectionFilters, '[note: %s ]', ['aboutThisTest'],
                function (element, next) {
                    expect(getUserSpecificSelectionFilters(element.criteria)).toEqual(element.expected)
                    next();
                })
        })
    })

    describe('getSelectionFiltersBySearchCriteria', () => {
        describe('handle errors', () => {
            it.each(resources.getSelectionFiltersBySearchCriteria, '[note: %s ]', ['aboutThisTest'],
                function (element, next) {
                    expect(getSelectionFiltersBySearchCriteria(element.criteria, element.view)).toEqual(element.expected)
                    next();
                })
        })
    })

    describe('getSelectionFiltersForMongoQuery', () => {
        it.each(resources.getSelectionFiltersForMongoQuery, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getSelectionFiltersForMongoQuery(element.findCriteria, element.sortCriteria, element.view)).toEqual(element.expected)
                next();
            })
    })
})