import { DOC_VIEW, LIST_VIEW } from '../../src/constants/EventTypes';
import Moment from 'moment'

const NONE_USER_SPECIFIC_FILTER_FOR_LIST_VIEW = {
    "$and": [
        {
            "$or": [
                { "type": "annotation" },
                { "type": "notification" }]
        }]
}

module.exports = {
    getUserSpecificSelectionFilters: [
        {
            aboutThisTest: "returns null when criteria is empty",
            criteria: [],
            expected: null
        },
        {
            aboutThisTest: "returns null when criterion.criteria is not set",
            criteria: [{
                // criteria: "Title",
                search: "title 1",
                selectText: "Title"
            }],
            expected: null
        },
        {
            aboutThisTest: "returns null when criterion.search is not set",
            criteria: [{
                criteria: "Title",
                //search: "title 1",
                selectText: "Title"
            }],
            expected: null
        },
        {
            aboutThisTest: "returns selection filter for a search based on title",
            criteria: [{
                criteria: "Title",
                search: "title 1",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "title":
                            { "$regex": ".*title 1.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on title in a CASE INSENSITIVE way on the searched term",
            criteria: [{
                criteria: "Title",
                search: "TITLE 1",
                selectText: "Title"
            }],
            view: LIST_VIEW,
            expected: {
                "$and": [
                    {
                        "title":
                            { "$regex": ".*TITLE 1.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on title in a CASE INSENSITIVE way on the criteria term",
            criteria: [{
                criteria: "TITLE",
                search: "title 1",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "title":
                            { "$regex": ".*title 1.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on author",
            criteria: [{
                criteria: "Author",
                search: "testName",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "username":
                            { "$regex": ".*testName.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on author in a CASE INSENSITIVE way on the searched term",
            criteria: [{
                criteria: "Author",
                search: "TESTNAME",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "username":
                            { "$regex": ".*TESTNAME.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on author in a CASE INSENSITIVE way on the criteria term",
            criteria: [{
                criteria: "AUTHOR",
                search: "testName",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "username":
                            { "$regex": ".*testName.*", "$options": "i" }
                    }]
            }
        },

        {
            aboutThisTest: "returns selection filter for a search based on type",
            criteria: [{
                criteria: "Type",
                search: "testType",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "type":
                            { "$regex": ".*testType.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on type in a CASE INSENSITIVE way on the searched term",
            criteria: [{
                criteria: "Type",
                search: "TESTTYPE",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "type":
                            { "$regex": ".*TESTTYPE.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on type in a CASE INSENSITIVE way on the criteria term",
            criteria: [{
                criteria: "TYPE",
                search: "testType",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "type":
                            { "$regex": ".*testType.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns null when criteria for date are not provided",
            criteria: [{
                criteria: "Creation date",
                //search: "before",
                date: "12 Sep 2018",
                selectText: "Creation date",
                momentDate: Moment('2018-09-12')
            }],
            expected: null
        },
        {
            aboutThisTest: "returns null when criteria for date are not provided",
            criteria: [{
                criteria: "Creation date",
                search: "before",
                date: "12 Sep 2018",
                selectText: "Creation date",
                //momentDate: Moment('2018-09-12')
            }],
            expected: null
        },
        {
            aboutThisTest: "Search based on starting date only",
            criteria: [{
                criteria: "Creation date",
                search: "before",
                date: "12 Sep 2018",
                selectText: "Creation date",
                momentDate: Moment.utc('2018-09-12')
            }],
            expected: {
                "$and": [
                    {
                        "creationDate": {
                            "$lt": '2018-09-12T00:00:00.000Z'
                        }
                    }
                ]
            }
        },
        {
            aboutThisTest: "Search based on ending date only",
            criteria: [{
                criteria: "Creation date",
                search: "after",
                date: "12 Sep 2018",
                selectText: "Creation date",
                momentDate: Moment.utc('2018-09-12')
            }],
            expected: {
                "$and": [
                    {
                        "creationDate": {
                            "$gte": '2018-09-12T00:00:00.000Z'
                        }
                    }
                ]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on content",
            criteria: [{
                criteria: "Content",
                search: "hello",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "content.text":
                            { "$regex": ".*hello.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on content in a CASE INSENSITIVE way on the searched term",
            criteria: [{
                criteria: "Content",
                search: "HELLO",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "content.text":
                            { "$regex": ".*HELLO.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on content in a CASE INSENSITIVE way on the criteria term",
            criteria: [{
                criteria: "CONTENT",
                search: "hello",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "content.text":
                            { "$regex": ".*hello.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on category",
            criteria: [{
                criteria: "Category",
                search: "error",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "category":
                            { "$regex": ".*error.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on category in a CASE INSENSITIVE way on the searched term",
            criteria: [{
                criteria: "Category",
                search: "ERROR",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "category":
                            { "$regex": ".*ERROR.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on category in a CASE INSENSITIVE way on the criteria term",
            criteria: [{
                criteria: "CATEGORY",
                search: "error",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "category":
                            { "$regex": ".*error.*", "$options": "i" }
                    }]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on everywhere",
            criteria: [{
                criteria: "Everywhere",
                search: "hello",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "$or": [
                            { "title": { "$regex": ".*hello.*", "$options": "i" } },
                            { "type": { "$regex": ".*hello.*", "$options": "i" } },
                            { "category": { "$regex": ".*hello.*", "$options": "i" } },
                            { "username": { "$regex": ".*hello.*", "$options": "i" } },
                            { "content.text": { "$regex": ".*hello.*", "$options": "i" } }
                        ]
                    }
                ]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on everywhere in a CASE INSENSITIVE way on the searched term",
            criteria: [{
                criteria: "Everywhere",
                search: "HELLO",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "$or": [
                            { "title": { "$regex": ".*HELLO.*", "$options": "i" } },
                            { "type": { "$regex": ".*HELLO.*", "$options": "i" } },
                            { "category": { "$regex": ".*HELLO.*", "$options": "i" } },
                            { "username": { "$regex": ".*HELLO.*", "$options": "i" } },
                            { "content.text": { "$regex": ".*HELLO.*", "$options": "i" } }
                        ]
                    }
                ]
            }
        },
        {
            aboutThisTest: "returns selection filter for a search based on everywhere in a CASE INSENSITIVE way on the criteria term",
            criteria: [{
                criteria: "EVERYWHERE",
                search: "hello",
                selectText: "Title"
            }],
            expected: {
                "$and": [
                    {
                        "$or": [
                            { "title": { "$regex": ".*hello.*", "$options": "i" } },
                            { "type": { "$regex": ".*hello.*", "$options": "i" } },
                            { "category": { "$regex": ".*hello.*", "$options": "i" } },
                            { "username": { "$regex": ".*hello.*", "$options": "i" } },
                            { "content.text": { "$regex": ".*hello.*", "$options": "i" } }
                        ]
                    }
                ]
            }
        },
        {
            aboutThisTest: "returns the filter for multi criteria search",
            criteria: [{
                criteria: "Content",
                search: "beamline",
            }, {
                criteria: "Category",
                search: "error",
            }],
            expected:
            {
                "$and": [
                    { "content.text": { "$regex": ".*beamline.*", "$options": "i" } },
                    { "category": { "$regex": ".*error.*", "$options": "i" } }
                ]
            }
        },

    ],

    getSelectionFiltersBySearchCriteria: [
        {
            aboutThisTest: "returns list view specific selection filter when no user criteria are provided",
            criteria: [],
            view: LIST_VIEW,
            expected: NONE_USER_SPECIFIC_FILTER_FOR_LIST_VIEW
        },
        {
            aboutThisTest: "returns the none user specific filter when criterion.criteria is not set",
            criteria: [{
                // criteria: "Title",
                search: "title 1",
                selectText: "Title"
            }],
            view: LIST_VIEW,
            expected: NONE_USER_SPECIFIC_FILTER_FOR_LIST_VIEW
        },
        {
            aboutThisTest: "returns the none user specific filter when criterion.search is not set",
            criteria: [{
                criteria: "Title",
                //search: "title 1",
                selectText: "Title"
            }],
            view: LIST_VIEW,
            expected: NONE_USER_SPECIFIC_FILTER_FOR_LIST_VIEW
        },
        {
            aboutThisTest: "returns the proper selection filters in the list view",
            criteria: [{
                criteria: "Title",
                search: "title 1",
                selectText: "Title"
            }],
            view: LIST_VIEW,
            expected: {
                "$and": [
                    {
                        "$or": [
                            { "type": "annotation" },
                            { "type": "notification" }
                        ]
                    },
                    {
                        "$and": [
                            {
                                "title":
                                    { "$regex": ".*title 1.*", "$options": "i" }
                            }]
                    }]
            }
        },
        {
            aboutThisTest: "returns the proper selection filters in the doc view",
            criteria: [{
                criteria: "Title",
                search: "title 1",
                selectText: "Title"
            }],
            view: DOC_VIEW,
            expected: {
                "$and": [
                    {
                        $or: [
                            { type: "annotation" },
                            {
                                $and: [
                                    { type: "notification" },
                                    { previousVersionEvent: { $ne: null } }
                                ]
                            }
                        ]
                    },
                    {
                        "$and": [
                            {
                                "title":
                                    { "$regex": ".*title 1.*", "$options": "i" }
                            }]
                    }]
            }
        },
    ],
    getSelectionFiltersForMongoQuery: [
        {
            aboutThisTest: 'returns mongo sleection filter using provided find and sort criteria',
            findCriteria: { "$and": [{ "$or": [{ "type": "annotation" }, { "type": "notification" }] }] },
            sortCriteria: { "createdAt": -1 },
            expected: { "find": { "$and": [{ "$or": [{ "type": "annotation" }, { "type": "notification" }] }] } }
        }
    ]
}


