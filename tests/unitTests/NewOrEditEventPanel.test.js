import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import NewOrEditEventPanel from '../../src/components/Logbook/NewOrEditEventPanel';

const resources = require('./resources/NewOrEditEventPanel.resource.js')

require('it-each')({ testPerIteration: true });

/* code executed before each test */
beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() })
})

describe('Unit tests on NewOrEditEventPanel', () => {
    function getShallowedWrapper(context, event, investigationId = "0", user = {
        sessionId: "testSessionId", username: 'testUsername'
    }) {
        return Enzyme.shallow(
            <NewOrEditEventPanel
                context={context}
                event={event}
                investigationId={investigationId}
                onEventUpdated={() => null}
                setEditEventVisibility={() => null}
                setNewEventVisibility={() => null}
                user={user}
                updateEvent={() => null}
            />);
    }

    describe('rendering', () => {
        it.each(resources.renderingEventHeader, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event).find('EventHeader').prop('text')).toEqual(element.expected);
                next();
            })

        it.each(resources.renderingEventContentPanel, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event, element.investigationId, element.user).find('EventContentPanel').prop('context')).toEqual(element.expected.context);
                expect(getShallowedWrapper(element.context, element.event, element.investigationId, element.user).find('EventContentPanel').prop('event')).toEqual(element.expected.event);
                expect(getShallowedWrapper(element.context, element.event, element.investigationId, element.user).find('EventContentPanel').prop('investigationId')).toEqual(element.expected.investigationId);
                expect(getShallowedWrapper(element.context, element.event, element.investigationId, element.user).find('EventContentPanel').prop('user')).toEqual(element.expected.user);
                next();

            })

        it.each(resources.renderingCreationDate, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event).find('CreationDate').prop('event')).toEqual(element.expected.event);
                next();
            })

        it.each(resources.renderingCommentBy, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event).find('CommentBy').prop('event')).toEqual(element.expected.event);
                next();
            })

        it.each(resources.renderingEventHistoryLabel, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event).find('EventHistoryLabel').prop('event')).toEqual(element.expected.event);
                next();
            })

        it.each(resources.renderingTagContainer, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event, element.investigationId).find('Connect(TagContainer)').prop('context')).toEqual(element.expected.context);
                expect(getShallowedWrapper(element.context, element.event, element.investigationId).find('Connect(TagContainer)').prop('event')).toEqual(element.expected.event);
                expect(getShallowedWrapper(element.context, element.event, element.investigationId).find('Connect(TagContainer)').prop('investigationId')).toEqual(element.expected.investigationId);
                next();
            })

        it.each(resources.renderingEventFooter, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.context, element.event).find('EventFooter')).toBeDefined();
                next();
            })



    })
})

// test("'in' property of the collapse component equals expanded prop", () => {
//     let myNewEvent2 = (isvisible) => {
//         return Enzyme.shallow(
//             <NewEvent
//                 investigationId="0"
//                 isVisible={isvisible}
//                 reloadEvents={() => null}
//                 setNewEventVisibility={() => null}
//                 user={myUser}
//             />)
//     }
//     expect(myNewEvent2(false).find(Collapse).prop("in")).toBe(false);
//     expect(myNewEvent2(true).find(Collapse).prop("in")).toBe(true);
// })





// test('Panel.body contains a formControl with prop type=text used to optionnally change the title', () => {
//     /* the expected node below is not the real node which is rendered. The real node contains and array function 
// which can not be tested with the contains method. */
//     let expectedNode = <FormControl type="text" placeholder="Write a title here (optional)" />
//     expect(myNewEventShallowedWrapper.find(Panel.Body).containsMatchingElement(expectedNode)).toBe(true);
// })



// test('createEvent function is called when a new event is created', () => {
//     let expectedFunction = myNewEventShallowedWrapper.instance().createEvent
//     expect(myNewEventShallowedWrapper.find(EventFooter).prop("onSaveButtonClicked")).toEqual(expectedFunction);

// })

// describe("A new event is created", () => {

//     let myNewEventMounted;

//     beforeEach(() => {
//         myNewEventMounted = Enzyme.mount(
//             <NewEvent
//                 user={myUser}
//                 expanded={false}
//                 investigationId="0"
//                 onCancelNewEventClicked={() => null}
//                 onSaveNewEventClicked={() => null}
//             />)
//     })

    // test('investigationId, creationDate, type, category, username are properly filled', () => {
    //     // create a mock function for sendNewEventToServer
    //     myNewEventMounted.instance().sendNewEventToServer = jest.fn()
    //     myNewEventMounted.update();

    //     // check that sendNewEventToServer() has been called
    //     myNewEventMounted.instance().createEvent();
    //     expect(myNewEventMounted.instance().sendNewEventToServer).toHaveBeenCalledTimes(1)

    //     // check that the expected new event is created.
    //     let expectedNewEvent = {
    //         investigationId: "0",
    //         content: [
    //             {
    //                 format: "plain",
    //                 text: null
    //             },
    //             {
    //                 format: "html",
    //                 text: null
    //             }
    //         ],
    //         creationDate: Date(),
    //         type: 'annotation',
    //         category: 'comment',
    //         title: "",
    //         username: 'testUsername'
    //     }
    //     expect(myNewEventMounted.instance().sendNewEventToServer).toHaveBeenCalledWith(expectedNewEvent)
    // })

    // test('The new title (not "") provided by the user is saved', () => {
    //     // create a mock function for sendNewEventToServer
    //     myNewEventMounted.instance().sendNewEventToServer = jest.fn()
    //     myNewEventMounted.update();

    //     // mimic the user typing some text in the input as a new title for the event.
    //     myNewEventMounted.find("input").instance().value = 'new title typed by the user'

    //     // call the method which creates the new event
    //     myNewEventMounted.instance().createEvent();


    //     // check that the expected new event is created.
    //     let expectedNewEvent = {
    //         investigationId: "0",
    //         content: [
    //             {
    //                 format: "plain",
    //                 text: null
    //             },
    //             {
    //                 format: "html",
    //                 text: null
    //             }
    //         ],
    //         creationDate: Date(),
    //         type: 'annotation',
    //         category: 'comment',
    //         title: 'new title typed by the user',
    //         username: 'testUsername'
    //     }
    //     expect(myNewEventMounted.instance().sendNewEventToServer).toHaveBeenCalledWith(expectedNewEvent)
    // })

// })
