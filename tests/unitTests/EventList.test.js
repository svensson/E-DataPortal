import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import EventList, { Event } from '../../src/components/Logbook/List/EventList';

const resources = require('./resources/eventList.resource.js')

require('it-each')({ testPerIteration: true });

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });
})

describe("Unit tests on EventList", () => {
    describe("Rendering", () => {
        function getShallowedWrapper(events) {
            return Enzyme.shallow(<EventList
                events={events}
                onEventClicked={() => null}
            />)
        }
    })
});

describe('Unit tests on Event', () => {
    describe('Rendering', () => {
        function getEventWrapper(
            availableTags = [],
            event,
            logbookContext,
            onEventClicked = () => null) {
            return Enzyme.shallow(<Event
                availableTags={availableTags}
                event={event}
                logbookContext={logbookContext}
                onEventClicked={onEventClicked}
            />
            )
        }

        it.each(resources.renderinglinkToEditEvent, '[renderinglinkToEditEvent: %s ]', ['aboutThisTest'],
            function (element, next) {
                let u = undefined;
                expect(getEventWrapper(u, element.event, element.logbookContext).find('Glyphicon').prop('glyph')).toEqual(element.expected);
                next();
            })
    })
})