import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import NewlyAvailableEventsDialogue from '../../src/components/Logbook/Menu/NewlyAvailableEventsDialogue';

const resources = require('./resources/newlyAvailableEventsDialogue.resource.js')

require('it-each')({ testPerIteration: true });

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() })
})

describe("NewlyAvailableEventsDialogue unit tests", () => {

    function getShallowedWrapper(eventCountSinceLastRefresh, latestEvents) {
        return Enzyme.shallow(<NewlyAvailableEventsDialogue
            eventCountSinceLastRefresh={eventCountSinceLastRefresh}
            latestEvents={latestEvents}
            onIconClicked={jest.fn()}
        />)
    };

    describe("rendering", () => {
        it.each(resources.rendering, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.eventCountSinceLastRefresh, element.latestEvents).find('div').props().children[1]).toEqual(element.expected);
                next();
            })
    })

    describe("rendering null", () => {
        it.each(resources.renderingNull, '[note: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.eventCountSinceLastRefresh, element.latestEvents).exists('div')).toEqual(false);
                next();
            })
    })

    describe('testing callbacks', () => {
        it('onIConClicked', () => {

            const wrapper = getShallowedWrapper(0, resources.callback);
            expect(wrapper.instance().props.onIconClicked).toHaveBeenCalledTimes(0);
            wrapper.find('Glyphicon').simulate('click');
            expect(wrapper.instance().props.onIconClicked).toHaveBeenCalledTimes(1);
        })
    })
})