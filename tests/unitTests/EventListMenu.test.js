import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import React from 'react';
import EventListMenu from '../../src/components/Logbook/Menu/EventListMenu';
import { LIST_VIEW } from '../../src/constants/EventTypes';

const resources = require('./resources/EventListMenu.resource.js')

require('it-each')({ testPerIteration: true });


beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });
})

describe("EventListMenu unit tests", () => {
    function getWrapper(
        availableTags = [],
        eventCountSinceLastRefresh = 0,
        getEvents = () => null,
        investigationId = 'testInvestigationId',
        isEventListAutorefreshEnabled = false,
        isNewButtonEnabled = true,
        logbookContext = {
            name: null,
            isReleased: null
        },
        numberOfMatchingEventsFound,
        reverseEventsSortingByCreationDate = () => null,
        searchEvents = () => null,
        selectionFilter = {},
        sessionId = 'sessionIdForTests',
        setEventListAutorefresh = () => null,
        setNewEventVisibility = () => null,
        setView = () => null,
        sortingFilter = {},
        view = LIST_VIEW
    ) {
        return Enzyme.shallow(<EventListMenu
            availableTags={availableTags}
            eventCountSinceLastRefresh={eventCountSinceLastRefresh}
            getEvents={getEvents}
            investigationId={investigationId}
            isEventListAutorefreshEnabled={isEventListAutorefreshEnabled}
            isNewButtonEnabled={isNewButtonEnabled}
            logbookContext={logbookContext}
            numberOfMatchingEventsFound={numberOfMatchingEventsFound}
            reverseEventsSortingByCreationDate={reverseEventsSortingByCreationDate}
            searchEvents={searchEvents}
            selectionFilter={selectionFilter}
            sessionId={sessionId}
            setEventListAutorefresh={setEventListAutorefresh}
            setNewEventVisibility={setNewEventVisibility}
            setView={setView}
            sortingFilter={sortingFilter}
            view={view} />)
    }
    describe("rendering", () => {
        let u = undefined;

        it.each(resources.renderingNewButton, '[renderingNewButton: %s ]', ['aboutThisTest'],
            function (element, next) {
                let NewButtonWrapper = getWrapper(u, u, u, u, u, element.isNewButtonEnabled, element.logbookContext)
                    .findWhere(n => n.prop('text') === 'New');

                expect(NewButtonWrapper.prop('glyph')).toEqual(element.expected.glyph);
                expect(NewButtonWrapper.prop('isEnabled')).toEqual(element.expected.isEnabled);
                expect(NewButtonWrapper.prop('isVisible')).toEqual(element.expected.isVisible);
                expect(NewButtonWrapper.prop('tooltipText')).toEqual(element.expected.tooltipText);
                next();
            })

        it.each(resources.renderingTakeAPhotoButton, '[renderingTakeAPhotoButton: %s ]', ['aboutThisTest'],
            function (element, next) {
                let NewButtonWrapper = getWrapper(u, u, u, u, u, u, element.logbookContext)
                    .findWhere(n => n.prop('text') === 'Take a photo');

                expect(NewButtonWrapper.prop('glyph')).toEqual(element.expected.glyph);
                expect(NewButtonWrapper.prop('isEnabled')).toEqual(element.expected.isEnabled);
                expect(NewButtonWrapper.prop('isVisible')).toEqual(element.expected.isVisible);
                expect(NewButtonWrapper.prop('tooltipText')).toEqual(element.expected.tooltipText);
                next();
            })

        it.each(resources.renderingPDFButton, '[renderingPDFButton: %s ]', ['aboutThisTest'],
            function (element, next) {
                let PDFButtonWrapper = getWrapper(u, u, u, u, u, element.isNewButtonEnabled, element.logbookContext, element.numberOfMatchingEventsFound)
                    .findWhere(n => n.prop('text') === 'PDF');

                expect(PDFButtonWrapper.prop('glyph')).toEqual(element.expected.glyph);
                expect(PDFButtonWrapper.prop('isEnabled')).toEqual(element.expected.isEnabled);
                expect(PDFButtonWrapper.prop('tooltipText')).toEqual(element.expected.tooltipText);
                next();
            });

        it.each(resources.renderingAutoRefreshMenuItem, '[renderingAutoRefreshMenuItem: %s ]', ['aboutThisTest'],
            function (element, next) {
                let menuItemWrapper = getWrapper(u, u, u, u, element.isEventListAutorefreshEnabled, u, element.logbookContext)
                    .findWhere(n => n.prop('eventKey') === 3.5);

                if (element.logbookContext.isReleased === true) {
                    expect(menuItemWrapper).not.toBeUndefined();
                } else {
                    expect(menuItemWrapper.childAt(0).prop('children')).toEqual(element.expected.text)
                }
                next();
            });
    })
})