import { NEW_EVENT_CONTEXT, EDIT_EVENT_CONTEXT, LOGBOOK_CONTEXT_NAME_PROPOSAL } from '../../../src/constants/EventTypes.js';
import { goodComment1 } from '../../GoodEventLibrary.js';

module.exports = {
    renderingNewButton: [
        {
            aboutThisTest: "logbook is not public, no New Event Panel",
            isNewButtonEnabled: true,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },

            expected: {
                glyph: 'plus',
                isEnabled: true,
                isVisible: true,
                text: 'New',
                tooltipText: 'Create a new event',
            }
        },
        {
            aboutThisTest: "logbook is not public, new button not enabled",
            isNewButtonEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },

            expected: {
                glyph: 'plus',
                isEnabled: false,
                isVisible: true,
                text: 'New',
                tooltipText: 'Create a new event',
            }
        },
        {
            aboutThisTest: "logbook is public, new button not visible",
            isNewButtonEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true
            },

            expected: {
                glyph: 'plus',
                isEnabled: false,
                isVisible: false,
                text: 'New',
                tooltipText: 'Create a new event',
            }
        },

    ],
    renderingTakeAPhotoButton: [
        {
            aboutThisTest: "logbook is not public",
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false,
            },
            expected: {
                glyph: 'camera',
                isEnabled: true,
                isVisible: true,
                tooltipText: 'Take a photo'
            }
        },
        {
            aboutThisTest: "logbook is public",
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true,
            },
            expected: {
                glyph: 'camera',
                isEnabled: true,
                isVisible: false,
                tooltipText: 'Take a photo'
            }
        }
    ],
    renderingPDFButton: [
        {
            aboutThisTest: "logbook is not public, logbook is empty, no new Event Panel opened",
            isNewButtonEnabled: true,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },
            numberOfMatchingEventsFound: 0,

            expected: {
                glyph: 'download',
                isEnabled: false,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
        {
            aboutThisTest: "logbook is not public, logbook is not empty, no new Event Panel opened",
            isNewButtonEnabled: true,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },
            numberOfMatchingEventsFound: 2,

            expected: {
                glyph: 'download',
                isEnabled: true,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
        {
            aboutThisTest: "logbook is not public, logbook is not empty, a new Event Panel is opened",
            isNewButtonEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },
            numberOfMatchingEventsFound: 2,

            expected: {
                glyph: 'download',
                isEnabled: false,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
        {
            aboutThisTest: "logbook is not public, logbook is empty, a new Event Panel is opened",
            isNewButtonEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },
            numberOfMatchingEventsFound: 0,

            expected: {
                glyph: 'download',
                isEnabled: false,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },


        {
            aboutThisTest: "logbook is public, logbook is empty, no new Event Panel opened",
            isNewButtonEnabled: true,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true
            },
            numberOfMatchingEventsFound: 0,

            expected: {
                glyph: 'download',
                isEnabled: false,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
        {
            aboutThisTest: "logbook is public, logbook is not empty, no new Event Panel opened",
            isNewButtonEnabled: true,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true
            },
            numberOfMatchingEventsFound: 2,

            expected: {
                glyph: 'download',
                isEnabled: true,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
        {
            aboutThisTest: "logbook is public, logbook is not empty, a new Event Panel is opened",
            isNewButtonEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true
            },
            numberOfMatchingEventsFound: 2,

            expected: {
                glyph: 'download',
                isEnabled: false,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
        {
            aboutThisTest: "logbook is public, logbook is empty, a new Event Panel is opened",
            isNewButtonEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true
            },
            numberOfMatchingEventsFound: 0,

            expected: {
                glyph: 'download',
                isEnabled: false,
                text: 'PDF',
                tooltipText: 'Download as PDF',
            }
        },
    ],
    renderingAutoRefreshMenuItem: [
        {
            aboutThisTest: 'logbook is public',
            isEventListAutorefreshEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: true
            },
            expected: {}
        },
        {
            aboutThisTest: 'logbook is not public, is not automatically refreshing',
            isEventListAutorefreshEnabled: false,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },
            expected: {
                text: 'Start refreshing logs'
            }
        },
        {
            aboutThisTest: 'logbook is not public, is automatically refreshing',
            isEventListAutorefreshEnabled: true,
            logbookContext: {
                name: LOGBOOK_CONTEXT_NAME_PROPOSAL,
                isReleased: false
            },
            expected: {
                text: 'Stop refreshing logs'
            }
        },

    ]
}
