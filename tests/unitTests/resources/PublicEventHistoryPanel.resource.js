import { LATEST_EVENT_VERSION, ORIGINAL_EVENT_VERSION } from '../../../src/constants/EventTypes.js';
import { goodComment1, goodComment4, goodComment7, goodError1, goodError3_OriginalVersion, goodError3, goodError4, goodError4_OriginalVersion } from '../../GoodEventLibrary.js';

module.exports = {
    renderingEventHeader: [
        {
            aboutThisTest: "check none functional props are passed to EventHeader",
            availableTags: [],
            event: goodComment1,

            expected: 'Public log details'
        },
    ],
    renderingParagraphWhenEventHasASingleVersion: [
        {
            aboutThisTest: 'paragraph is displayed when the event has only a single version',
            availableTags: [],
            event: goodComment1,

            expected: {
                paragraphCount: 1,
                paragraphText: 'This event has never been commented.'
            }
        },
        {
            aboutThisTest: 'paragraph is not displayed when the event has more that one version (here 2 versions)',
            availableTags: [],
            event: goodComment4,

            expected: {
                paragraphCount: 0,
            }
        },
        {
            aboutThisTest: 'paragraph is not displayed when the event has more that one version (here 3 versions)',
            availableTags: [],
            event: goodComment7,

            expected: {
                paragraphCount: 0,
            }
        },
    ],
    renderingEventVersionPanel: [
        {
            aboutThisTest: 'Annotation which has never been commented',
            availableTags: [],
            event: goodComment1,

            expected: {
                eventVersionPanelCount: 1,
                first: {
                    availableTags: [],
                    event: goodComment1,
                    version: LATEST_EVENT_VERSION
                }
            }
        },
        {
            aboutThisTest: 'Annotation which has been commented',
            availableTags: [],
            event: goodComment4,

            expected: {
                eventVersionPanelCount: 1,
                first: {
                    availableTags: [],
                    event: goodComment4,
                    version: LATEST_EVENT_VERSION
                },
            }
        },
        {
            aboutThisTest: 'Notification which has never been commented',
            availableTags: [],
            event: goodError1,

            expected: {
                eventVersionPanelCount: 1,
                first: {
                    availableTags: [],
                    event: goodError1,
                    version: LATEST_EVENT_VERSION
                }
            }
        },
        {
            aboutThisTest: 'Notification which has been commented',
            availableTags: [],
            event: goodError3,

            expected: {
                eventVersionPanelCount: 2,
                first: {
                    availableTags: [],
                    event: goodError3,
                    version: LATEST_EVENT_VERSION
                },
                second: {
                    availableTags: [],
                    event: goodError3_OriginalVersion,
                    version: ORIGINAL_EVENT_VERSION
                }
            }
        },
        {
            aboutThisTest: 'Notification which has been commented 2 times',
            availableTags: [],
            event: goodError4,

            expected: {
                eventVersionPanelCount: 2,
                first: {
                    availableTags: [],
                    event: goodError4,
                    version: LATEST_EVENT_VERSION
                },
                second: {
                    availableTags: [],
                    event: goodError4_OriginalVersion,
                    version: ORIGINAL_EVENT_VERSION
                }
            }
        }
    ],
    renderingEventFooter: [
        {
            aboutThisTest: 'check eventFooter is rendered',
            availableTags: [],
            event: goodComment1,
            expected: {
                isSaveButtonEnabled: false,
                isSaveButtonVisible: false,
                buttonLabel: 'Close',
            }
        }
    ],
    testCallbackRenderingFunctions: {
        onCancelButtonClicked: {
            availableTags: [],
            event: goodComment1,
        }
    }


}
