module.exports = {
    rendering: [
        {
            aboutThisTest: "test 4",
            latestEvents: { data: [getEvent('id1'), getEvent('id2'), getEvent('id3')] },
            eventCountSinceLastRefresh: 1,
            expected: 2
        },
    ],

    renderingNull: [
        {
            aboutThisTest: "test 1",
            latestEvents: null,
            eventCountSinceLastRefresh: null,
            expected: 0
        },
        {
            aboutThisTest: "test 2",
            latestEvents: { data: null },
            eventCountSinceLastRefresh: null,
            expected: 0
        },
        {
            aboutThisTest: "test 3",
            latestEvents: { data: [getEvent('id1'), getEvent('id2')] },
            eventCountSinceLastRefresh: null,
            expected: 0
        },
        {
            aboutThisTest: "test 5",
            latestEvents: { data: [getEvent('id1'), getEvent('id2'), getEvent('id3')] },
            eventCountSinceLastRefresh: 4,
            expected: 0
        },
    ],

    callback: { data: [getEvent('id1')] }

};

/* return an event object which ID is eventId*/
function getEvent(eventId) {
    return {
        _id: eventId,
        category: "comment",
        content: [
            { format: 'plainText', text: 'this is a test' },
            { format: 'html', text: '<p> this is a test </p>' }
        ],
        createdAt: '2018-01-01T00:01:00.000Z',
        creationDate: '2018-01-01T00:00:00.000Z',
        datasetId: null,
        file: [],
        fileSize: null,
        filename: null,
        investigationId: 'testInvestigationId',
        machine: null,
        previousVersionEvent: null,
        software: null,
        tag: [],
        title: 'test title',
        type: "annotation",
        updatedAt: "2018-01-01T00:00:00.000Z",
        username: "mchaille"
    }
}