import { goodComment1, goodComment4 } from '../../GoodEventLibrary';
import { LOGBOOK_CONTAINER_CONTEXT } from '../../../src/constants/EventTypes';

module.exports = {
    renderinglinkToEditEvent: [
        {
            aboutThisTest: "logbook is not public; event has no previous version",
            event: goodComment1,
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
            expected: 'plus'
        },
        {
            aboutThisTest: "logbook is not public; event has one previous version",
            event: goodComment4,
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
            expected: 'edit'
        },
        {
            aboutThisTest: "logbook is public",
            event: goodComment4,
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
            expected: 'eye-open'
        },
    ]
}