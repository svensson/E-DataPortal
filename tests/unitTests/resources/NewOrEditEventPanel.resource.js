import { NEW_EVENT_CONTEXT, EDIT_EVENT_CONTEXT } from '../../../src/constants/EventTypes.js';
import { goodComment1 } from '../../GoodEventLibrary.js';

module.exports = {
    renderingEventHeader: [
        {
            aboutThisTest: "simulate new event creation",
            context: NEW_EVENT_CONTEXT,
            event: null,

            expected: 'New comment'
        },
        {
            aboutThisTest: "edit an event",
            context: EDIT_EVENT_CONTEXT,
            event: goodComment1,

            expected: 'Edit'
        },
    ],
    renderingEventContentPanel: [
        {
            aboutThisTest: "check none functional props are passed to EventContent panel for new event creation",
            context: NEW_EVENT_CONTEXT,
            event: null,
            investigationId: '0',
            user: {
                sessionId: "testSessionId",
                username: 'testUsername'
            },

            expected: {
                context: NEW_EVENT_CONTEXT,
                event: null,
                investigationId: '0',
                user: {
                    sessionId: "testSessionId",
                    username: 'testUsername'
                }
            }
        },
        {
            aboutThisTest: "check none functional props are passed to EventContent panel for event edition",
            context: EDIT_EVENT_CONTEXT,
            event: goodComment1,
            investigationId: '0',
            user: {
                sessionId: "testSessionId",
                username: 'testUsername'
            },

            expected: {
                context: EDIT_EVENT_CONTEXT,
                event: goodComment1,
                investigationId: '0',
                user: {
                    sessionId: "testSessionId",
                    username: 'testUsername'
                }
            }
        },
    ],
    renderingCreationDate: [
        {
            aboutThisTest: "check none functional props are passed to CreationDate panel for new event creation",
            context: NEW_EVENT_CONTEXT,
            event: null,
            expected: {
                event: null,
            }
        },
        {
            aboutThisTest: "check none functional props are passed to CreationDate panel for event edition",
            context: EDIT_EVENT_CONTEXT,
            event: goodComment1,
            expected: {
                event: goodComment1,
            }
        },
    ],
    renderingCommentBy: [
        {
            aboutThisTest: "check none functional props are passed to commentBy panel for new event creation",
            context: NEW_EVENT_CONTEXT,
            event: null,
            expected: {
                event: null,
            }
        },
        {
            aboutThisTest: "check none functional props are passed to CommentBy panel for event edition",
            context: EDIT_EVENT_CONTEXT,
            event: goodComment1,
            expected: {
                event: goodComment1,
            }
        },
    ],
    renderingEventHistoryLabel: [
        {
            aboutThisTest: "check none functional props are passed to EventHistoryLabel panel for new event creation",
            context: NEW_EVENT_CONTEXT,
            event: null,
            expected: {
                event: null,
            }
        },
        {
            aboutThisTest: "check none functional props are passed to EventHistoryLabel panel for event edition",
            context: EDIT_EVENT_CONTEXT,
            event: goodComment1,
            expected: {
                event: goodComment1,
            }
        },
    ],
    renderingTagContainer: [
        {
            aboutThisTest: "check none functional props are passed to TagContainer panel for new event creation",
            context: NEW_EVENT_CONTEXT,
            event: null,
            investigationId: "0",
            expected: {
                context: NEW_EVENT_CONTEXT,
                event: null,
                investigationId: "0"
            }
        },
        {
            aboutThisTest: "check none functional props are passed to EventHistoryLabel panel for event edition",
            context: EDIT_EVENT_CONTEXT,
            event: goodComment1,
            investigationId: '0',
            expected: {
                context: EDIT_EVENT_CONTEXT,
                event: goodComment1,
                investigationId: '0'
            }
        },
    ],
    renderingEventFooter: [
        {
            aboutThisTest: 'check eventFooter is rendered for new event creation',
            context: NEW_EVENT_CONTEXT,
        },
        {
            aboutThisTest: 'check eventFooter is rendered for event edition',
            context: EDIT_EVENT_CONTEXT,
        },
    ],


}
