import { LOGBOOK_CONTAINER_CONTEXT, INFO_MESSAGE_TYPE } from '../../../src/constants/EventTypes';
import { goodComment1, goodComment2 } from '../../GoodEventLibrary';

module.exports = {
    renderingEventListOrEmptyLogbookMessage: [
        {
            aboutThisTest: 'logbook is not public, has no events',
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
            serverResponse1: {
                data: []
            },
            serverResponse2: { data: '0' },
            expected: {
                showsEventList: false,
                userMessageCount: 2,
                userMessageProps: {
                    message: 'The logbook is empty.',
                    type: INFO_MESSAGE_TYPE,
                    isTextCentered: true
                }
            }
        },
        {
            aboutThisTest: 'logbook is not public, has events',
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
            serverResponse1: {
                data: [goodComment1]
            },
            serverResponse2: { data: '1' },
            expected: {
                showsEventList: true,
                userMessageCount: 1,
                userMessageProps: {
                    message: null,
                    type: null,
                    isTextCentered: null
                }
            }
        },
        {
            aboutThisTest: 'logbook is public, has no events',
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
            serverResponse1: {
                data: []
            },
            serverResponse2: { data: '0' },
            expected: {
                showsEventList: false,
                userMessageCount: 2,
                userMessageProps: {
                    message: 'The logbook is empty.',
                    type: INFO_MESSAGE_TYPE,
                    isTextCentered: true
                }
            }
        },
        {
            aboutThisTest: 'logbook is public, has events',
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
            serverResponse1: {
                data: [goodComment1]
            },
            serverResponse2: { data: '1' },
            expected: {
                showsEventList: true,
                userMessageCount: 1,
                userMessageProps: {
                    message: null,
                    type: null,
                    isTextCentered: null
                }
            }
        }
    ],
    renderingPublicEventHistoryPanel: [
        {
            aboutThisTest: 'logbook is public',
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: true },
            serverResponse1: {
                data: [goodComment1]
            },
            serverResponse2: { data: '1' },
            expected: {
                showsPublicEventHistory: true
            }
        },
        {
            aboutThisTest: 'logbook is not public',
            logbookContext: { name: LOGBOOK_CONTAINER_CONTEXT, isReleased: false },
            serverResponse1: {
                data: [goodComment1]
            },
            serverResponse2: { data: '1' },
            expected: {
                showsPublicEventHistory: false
            }
        }

    ]
}