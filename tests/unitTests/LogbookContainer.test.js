import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
const IORequestModule = require('../../src/containers/Logbook/IORequests')
import { LogbookContainer } from '../../src/containers/Logbook/LogbookContainer';

import { GUI_CONFIG } from '../../src/config/gui.config';
import { LOGGED_IN } from '../../src/constants/ActionTypes';
jest.mock("../../src/config/gui.config");

require('it-each')({ testPerIteration: true });

const resources = require('./resources/LogbookContainer.resource.js');

beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() });
})

/* In these tests, redux store is not used. Tests involving the redux store should go in integration tests folder*/
describe('Unit tests on LogbookContainer', () => {
    function getWrapper(
        availableTags = [],
        logbookContext,
    ) {
        /* overrides default configuration */
        GUI_CONFIG.mockReturnValue({
            EVENTS_PER_PAGE: 2,
            DEFAULT_SORTING_FILTER: {
                createdAt: -1  // temporary workaround. createdAt must be replaced by creationDate
            },
            AUTOREFRESH_DELAY: 100,
            AUTOREFRESH_ENABLED: false
        })

        return Enzyme.shallow(<LogbookContainer
            investigationId='testInvestigationId'
            /* redux props (redux not used here) */
            availableTags={availableTags}
            clearAvailableTags={() => null}
            logbookContext={logbookContext}
            setLogbookContext={() => null}
            user={{ type: LOGGED_IN, username: 'username', sessionId: 'sessionId' }}
        />);
    }

    describe('Rendering', () => {
        let u = undefined;

        it.each(resources.renderingEventListOrEmptyLogbookMessage, '[renderingEventListOrEmptyLogbookMessage: %s ]', ['aboutThisTest'],
            function (element, next) {
                // mock IO callback of initialization which return immediately.
                IORequestModule.getEventsByInvestigationId = jest.fn(
                    (sessionId, investigationId, eventsPerDownload, offset, selectionFilter, sortingFilter, onSuccess, onFailure) => { onSuccess(element.serverResponse1) });
                IORequestModule.getEventCountByInvestigationId = jest.fn(
                    (sessionId, investigationId, selectionFilter, onSuccess, onFailure) => { onSuccess(element.serverResponse2) });

                let wrapper = getWrapper(u, element.logbookContext);

                if (element.expected.showsEventList === true) {
                    expect(wrapper.exists('EventList')).toBe(true)
                }

                if (element.expected.showsEventList === false) {
                    expect(wrapper.exists('EventList')).toBe(false)
                }

                expect(wrapper.find('UserMessage').length).toBe(element.expected.userMessageCount)
                if (element.userMessageCount === 2) {
                    expect(wrapper.find('UserMessage').at(1).prop('message')).toEqual(element.expected.userMessageProps.message)
                    expect(wrapper.find('UserMessage').at(1).prop('type')).toEqual(element.expected.userMessageProps.type)
                    expect(wrapper.find('UserMessage').at(1).prop('isTextCentered')).toEqual(element.expected.userMessageProps.isTextCentered)
                }
                next();
            })


        it.each(resources.renderingPublicEventHistoryPanel, '[renderingPublicEventHistoryPanel: %s ]', ['aboutThisTest'],
            function (element, next) {
                // mock IO callback of initialization which return immediately.
                IORequestModule.getEventsByInvestigationId = jest.fn(
                    (sessionId, investigationId, eventsPerDownload, offset, selectionFilter, sortingFilter, onSuccess, onFailure) => { onSuccess(element.serverResponse1) });
                IORequestModule.getEventCountByInvestigationId = jest.fn(
                    (sessionId, investigationId, selectionFilter, onSuccess, onFailure) => { onSuccess(element.serverResponse2) });

                let wrapper = getWrapper(u, element.logbookContext);
                if (element.expected.showsPublicEventHistory === true) {
                    expect(wrapper.find('OverlayBox').at(1).exists('PublicEventHistoryPanel')).toBe(true)
                }
                if (element.expected.showsPublicEventHistory === false) {
                    expect(wrapper.find('OverlayBox').at(1).exists('PublicEventHistoryPanel')).toBe(false)
                }

                next();
            })
    })
})