import React from 'react'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import PublicEventHistoryPanel from '../../src/components/Logbook/PublicEventHistoryPanel';
import { PUBLIC_EVENT_DETAILS_INVISIBLE } from '../../src/constants/EventTypes';

const resources = require('./resources/PublicEventHistoryPanel.resource.js')

require('it-each')({ testPerIteration: true });

/* code executed before each test */
beforeEach(() => {
    Enzyme.configure({ adapter: new Adapter() })
})

describe('Unit tests on PublicEventHistoryPanel', () => {
    function getShallowedWrapper(availableTags, event, setPublicEventVisibility = () => null) {
        return Enzyme.shallow(
            <PublicEventHistoryPanel
                availableTags={availableTags}
                event={event}
                setPublicEventVisibility={setPublicEventVisibility}
            />);
    }

    describe('rendering', () => {
        it.each(resources.renderingEventHeader, '[renderingEventHeader: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.availableTags, element.event).find('EventHeader').prop('text')).toEqual(element.expected);
                next();
            })

        it.each(resources.renderingParagraphWhenEventHasASingleVersion, '[renderingParagraphWhenEventHasASingleVersion: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.availableTags, element.event).find('p').length).toBe(element.expected.paragraphCount);
                if (element.expected.paragraphCount != 0) {
                    expect(getShallowedWrapper(element.availableTags, element.event).find('p').prop('children')).toEqual(element.expected.paragraphText);
                }
                next();
            })

        it.each(resources.renderingEventVersionPanel, '[renderingEventVersionPanel: %s ]', ['aboutThisTest'],
            function (element, next) {
                expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').length).toBe(element.expected.eventVersionPanelCount)

                expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').at(0).prop('availableTags')).toEqual(element.expected.first.availableTags)
                expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').at(0).prop('event')).toEqual(element.expected.first.event)
                expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').at(0).prop('version')).toEqual(element.expected.first.version)
                if (element.expected.eventVersionPanelCount > 1) {
                    expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').at(1).prop('availableTags')).toEqual(element.expected.second.availableTags)
                    expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').at(1).prop('event')).toEqual(element.expected.second.event)
                    expect(getShallowedWrapper(element.availableTags, element.event).find('EventVersionPanel').at(1).prop('version')).toEqual(element.expected.second.version)
                }
                next();
            })

        it.each(resources.renderingEventFooter, '[renderingEventFooter: %s ]', ['aboutThisTest'],
            function (element, next) {
                const mockedOnCancelButtonClicked = jest.spyOn(PublicEventHistoryPanel.prototype, 'onCancelButtonClicked');
                let wrapper = getShallowedWrapper(element.availableTags, element.event);

                expect(wrapper.find('EventFooter').prop('isSaveButtonEnabled')).toBe(element.expected.isSaveButtonEnabled);
                expect(wrapper.find('EventFooter').prop('isSaveButtonVisible')).toBe(element.expected.isSaveButtonVisible);
                expect(wrapper.find('EventFooter').prop('cancelButtonLabel')).toEqual(element.expected.buttonLabel);
                wrapper.find('EventFooter').prop('onCancelButtonClicked')();
                expect(mockedOnCancelButtonClicked).toHaveBeenCalled();
                next();
            })
    })

    describe('Test callback triggering functions', () => {
        test('oncancelButtonClicked()', () => {
            let input = resources.testCallbackRenderingFunctions.onCancelButtonClicked;

            let mockedSetPublicEventVisibility = jest.fn();
            let wrapper = getShallowedWrapper(input.availableTags, input.event, mockedSetPublicEventVisibility);
            wrapper.instance().onCancelButtonClicked();
            expect(mockedSetPublicEventVisibility).toHaveBeenCalledWith(PUBLIC_EVENT_DETAILS_INVISIBLE);
        })
    })
})