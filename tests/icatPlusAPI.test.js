import { getEventsByInvestigationId, getEventCountByInvestigationId, updateEvent, createEvent, getSelectionFilterBySearchCriteria, uploadFile, getFileByEventId, getPDF } from "../src/api/icat/icatPlus.js"
import ICATPLUS from "../src/config/icat/icatPlus.js"

describe("Tests URL generation used to hit ICAT+", () => {
    describe("getEventsByInvestigationId()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";

            let expectedURL = ICATPLUS.server + "/logbook/mySessionId/investigation/id/0123456/event/query"
            expect(getEventsByInvestigationId(actualSessionId, actualInvestigationId)).toEqual(expectedURL)
        })
    })

    describe("getEventCountByInvestigationId()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";

            let expectedURL = ICATPLUS.server + "/logbook/mySessionId/investigation/id/0123456/event/count"
            expect(getEventCountByInvestigationId(actualSessionId, actualInvestigationId)).toEqual(expectedURL)
        })
    })

    describe("updateEvent()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";

            let expectedURL = ICATPLUS.server + "/logbook/mySessionId/investigation/id/0123456/event/update"
            expect(updateEvent(actualSessionId, actualInvestigationId)).toEqual(expectedURL)
        })
    })

    describe("createEvent()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";

            let expectedURL = ICATPLUS.server + "/logbook/mySessionId/investigation/id/0123456/event/create"
            expect(createEvent(actualSessionId, actualInvestigationId)).toEqual(expectedURL)
        })
    })

    describe("uploadFile()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";

            let expectedURL = ICATPLUS.server + "/resource/mySessionId/file/investigation/id/0123456/upload"
            expect(uploadFile(actualSessionId, actualInvestigationId)).toEqual(expectedURL)
        })
    })

    describe("getFileByEventId()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";
            let actualEventId = 'myEventId';

            let expectedURL = ICATPLUS.server + "/resource/mySessionId/file/id/myEventId/investigation/id/0123456/download"
            expect(getFileByEventId(actualSessionId, actualInvestigationId, actualEventId)).toEqual(expectedURL)
        })
    })

    describe("getPDF()", () => {
        it('returns http request when investigationId and sessionId parameters are provided', () => {
            let actualSessionId = "mySessionId";
            let actualInvestigationId = "0123456";
            let actualSelectionFilter = {
                find: {
                    $and: [{
                        $or: [
                            { type: "annotation" },
                            { type: "notification" }
                        ]
                    }]
                },
                sort: { createdAt: -1 },
                skip: 0,
                limit: 400
            };

            let expectedURL = ICATPLUS.server +
                "/logbook/mySessionId/investigation/id/0123456/event/pdf" +
                "?find=" + JSON.stringify(actualSelectionFilter.find) +
                "&sort=" + JSON.stringify(actualSelectionFilter.sort) +
                "&skip=" + JSON.stringify(actualSelectionFilter.skip) +
                "&limit=" + JSON.stringify(actualSelectionFilter.limit);
            expect(getPDF(actualSessionId, actualInvestigationId, actualSelectionFilter)).toEqual(expectedURL)
        })
    })
})