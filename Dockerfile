FROM node:alpine

ENV NODE_ENV production

COPY . /edataportal
WORKDIR /edataportal

RUN apk --no-cache add git curl \
  && npm install \
  && npm install -g serve

EXPOSE 5000

ENTRYPOINT ["/bin/sh", "/edataportal/entrypoint"]
CMD ["serve", "-s", "build"]

HEALTHCHECK --start-period=90s --timeout=5s --retries=1 CMD /edataportal/healthcheck
