import React, { Component } from 'react';
import { Badge,  Grid, Row, Col,   FormControl,  ButtonToolbar,  ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './InvestigationContainerMenu.css';


class InvestigationContainerMenu extends Component {
    handleChange(event) {
      
    }

    handleKeyPress(event) {
        if (event.key === 'Enter') {
            this.props.onSearch(event.target.value);
        }
    }

    render() {
        let logURL = "/investigation/" + this.props.investigationId;

        let datasetCount = 0;
        if (this.props.datasets.length) {
            datasetCount = this.props.datasets.length;
        }
        return (
            <Grid fluid className="navbar-  MenuBar">
                <Row>
                    <Col xs={6}>

                        <ButtonToolbar >
                            <ToggleButtonGroup style={{ marginTop: "5px" }} type="radio" name="options" value={this.props.perspective}>
                                <ToggleButton bsSize="small" value={1}>
                                    <Link to={{ pathname: logURL, search: "perspective=1" }}>
                                        <span className="glyphicon glyphicon-th-list"></span> Dataset List <Badge style={{ backgroundColor: '#007bff' }}> {datasetCount} </Badge></Link>


                                </ToggleButton>
                                <ToggleButton bsSize="small" value={2}>
                                    <Link to={{ pathname: logURL, search: "perspective=2" }}><span className="glyphicon glyphicon-folder-open"></span> Browse Files</Link>
                                </ToggleButton>

                                <ToggleButton bsSize="small" value={3}>
                                    <Link to={'/investigation/' + this.props.investigationId + '/events'} > <span className="glyphicon glyphicon-comment"></span> Logbook</Link>

                                </ToggleButton>
                            </ToggleButtonGroup>
                        </ButtonToolbar>

                    </Col>
                    <Col xs={4}>

                    </Col>

                    <Col xs={2}>
                        <FormControl bsSize="small" onKeyPress={this.handleKeyPress.bind(this)} onChange={this.handleChange.bind(this)} type="text" placeholder="Search and click Enter" style={{ marginTop: '5px', marginRight: '10px' }} />
                    </Col>
                </Row>
            </Grid>

        )
    }

}

export default InvestigationContainerMenu;
