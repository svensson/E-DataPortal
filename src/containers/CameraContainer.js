import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { setBreadCrumbs } from '../actions/breadcrumbs.js';
import Camera, { FACING_MODES } from 'react-html5-camera-photo';
import './CameraContainer.css';
import _ from "lodash";
import { createEventFromBase64 } from "../api/icat/icatPlus.js";

class CameraContainer extends Component {
    onTakePhoto(dataUri) {

        try {
            axios({
                method: "post",
                url: createEventFromBase64(this.props.user.sessionId, this.props.investigationId),
                data: { base64: dataUri }
            })
                .then(function (value) {                    
                    alert("Uploaded Successfully")
                    window.close();
                }, function (error) {
                    alert(error);
                });
        }
        catch (e) {
            alert(e);
        }
    }

    onCameraError(error) {
        console.error('onCameraError', error);
    }

    onCameraStart(stream) {
        console.log('onCameraStart');
    }

    onCameraStop() {
        console.log('onCameraStop');
    }



    render() {
        if ((!this.props.user) || (!this.props.user.sessionId)) {
            return null;
        }

        return (<
            Camera onTakePhoto={
                (dataUri) => {
                    this.onTakePhoto(dataUri);
                }
            }
            onCameraError={
                (error) => {
                    this.onCameraError(error);
                }
            }
            idealFacingMode={
                FACING_MODES.ENVIRONMENT
            }
            idealResolution={
                {
                    width: 640,
                    height: 480
                }
            }
            imageCompression={
                0.97
            }
            isMaxResolution={
                false
            }
            isFullscreen={
                false
            }
            isImageMirror={
                false
            }
            isDisplayStartCameraError={
                true
            }
            sizeFactor={
                1
            }
            onCameraStart={
                (stream) => {
                    this.onCameraStart(stream);
                }
            }
            onCameraStop={
                () => {
                    this.onCameraStop();
                }
            }
        />);
    }

    /**
     * TODO: This should be refactorized
     */
    componentDidMount() {
        if (this.props.investigationId) {
            let investigation = _.find(this.props.myInvestigations, (investigation) => {
                return investigation.id === this.props.investigationId
            });
            if (investigation) {
                // investigation was found in my Data
                this.props.setBreadCrumbs([{
                    name: 'My Data',
                    link: '/investigations'
                },
                {
                    name: 'Investigations',
                    link: '/investigations'
                },
                {
                    badge: investigation.name,
                    name: investigation.summary,
                    link: '/investigation/' + investigation.id + '/datasets'
                }
                ]);
            } else {
                // investion not found in my data, it can be among closed data
                investigation = _.find(this.props.investigations, (investigation) => {
                    return investigation.id === this.props.investigationId
                });
                if (investigation) {
                    // investigation was found in my Data
                    this.props.setBreadCrumbs([{
                        name: 'Closed Data',
                        link: '/closed'
                    },
                    {
                        name: 'Investigations',
                        link: '/closed'
                    },
                    {
                        badge: investigation.name,
                        name: investigation.summary,
                        link: '/investigation/' + investigation.id + '/datasets'
                    }
                    ]);
                }

            }
        }
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        investigations: state.investigations.data,
        myInvestigations: state.myInvestigations.data,
        releasedInvestigations: state.releasedInvestigations.data
    };
}

function mapDispatchToProps(dispatch) {
    return {
        setBreadCrumbs: breadcrumbs => dispatch(setBreadCrumbs(breadcrumbs))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CameraContainer);