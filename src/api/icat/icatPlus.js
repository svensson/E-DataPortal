import ICATPLUS from '../../config/icat/icatPlus.js'

/**
 * Get URL needed to retrieve events for a given investigation
 * @param {String} sessionId the session identifier
 * @param {String} investigationId the session identifier
 * @return {String} the URL to get the requested events. Null if investigationId or sessionId is missing.
 */
export function getEventsByInvestigationId(sessionId, investigationId) {
    return ICATPLUS.server + "/logbook/sessionId/investigation/id/investigationId/event/query"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

/**
 * Get URL needed to retrieve the event count for a given investigation
 * @param {string} sessionId the session identifier
 * @param {String} investigationId the investigation identifier
 * @return {String} the URL to get the requested event count
 */
export function getEventCountByInvestigationId(sessionId, investigationId) {
    return ICATPLUS.server + "/logbook/sessionId/investigation/id/investigationId/event/count"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

/**
* Get URL used to create a new event for a given investigation on ICAT+ 
* @param {*} investigationId investigation indentifier 
* @param {String} sessionId session identifier
* @return {String} URL to get the requested events
*/
export function createEvent(sessionId, investigationId) {
    return ICATPLUS.server + "/logbook/sessionId/investigation/id/investigationId/event/create"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

/**
 * Get URL used to update an event on a given investigation on ICAT+ 
 * @param {String} sessionId the session identifier
 * @param {String} investigationId the investigation indentifier 
 * @return {String} the URL to get the requested events
 */
export function updateEvent(sessionId, investigationId) {
    return ICATPLUS.server + "/logbook/sessionId/investigation/id/investigationId/event/update"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

/**
* Get URL used to upload data such as a file to ICAT+.
* @param {*} investigationId the investigationId
*/
export function uploadFile(sessionId, investigationId) {
    return ICATPLUS.server + "/resource/sessionId/file/investigation/id/investigationId/upload"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

/**
* Get URL used to retrieve the file of an event which category is 'file'
* @param {string} sessionId session identifier 
* @param {*} investigationId investigation identifier
* @param {string} eventId event identifier where the file is stored
* @return {String} URL to get the file
*/
export function getFileByEventId(sessionId, investigationId, eventId) {
    return ICATPLUS.server + "/resource/sessionId/file/id/eventId/investigation/id/investigationId/download"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId)
        .replace("eventId", eventId);
}

/**
* Get URL used to download a PDF file for a given investigation from the logbook
* @param {string} sessionId session identifier 
* @param {*} investigationId investigation identifier
* @param {object} selectionFilter selection filter used to retrieve part of the logbook. This is URI encoded and passed as query string
*/
export function getPDF(sessionId, investigationId, selectionFilter) {


    return ICATPLUS.server + "/logbook/sessionId/investigation/id/investigationId/event/pdf?find=&sort=&skip=&limit="
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId)
        .replace("find=", () => { return 'find=' + ((selectionFilter && selectionFilter.find) ? JSON.stringify(selectionFilter.find) : '') })
        .replace("&sort=", () => { return '&sort=' + ((selectionFilter && selectionFilter.sort) ? JSON.stringify(selectionFilter.sort) : '') })
        .replace("&skip=", () => { return '&skip=' + ((selectionFilter && selectionFilter.skip !== undefined) ? JSON.stringify(selectionFilter.skip) : '') })
        .replace("&limit=", () => { return '&limit=' + ((selectionFilter && selectionFilter.limit !== undefined) ? JSON.stringify(selectionFilter.limit) : '') });
}

/**
 * Get the URL used to mint a DOI
 * @param {*} sessionId session identifier
 * @return {string} URL used to mint a DOI
 */
export function getMintDOI(sessionId) {
    return ICATPLUS.server + "/doi/sessionId/mint"
        .replace("sessionId", sessionId);
}

/** Get the tags associated to a given investigation 
* @param {string} sessionId session identifier 
* @param {*} investigationId investigation identifier
*/
export function getTagsByInvestigationId(sessionId, investigationId) {
    return ICATPLUS.server + "/logbook/" + sessionId + "/investigation/id/" + investigationId + "/tag";
}

/** Create a given tag associated to a given investigation */
export function createTagsByInvestigationId(sessionId, investigationId) {
    return ICATPLUS.server + '/logbook/' + sessionId + '/investigation/id/' + investigationId + '/tag/create';
}

/** Update a given tag associated to a given investigation */
export function updateTagsByInvestigationId(sessionId, investigationId, tagId) {
    return ICATPLUS.server + "/logbook/" + sessionId + "/investigation/id/" + investigationId + "/tag/id/" + tagId + "/tag/update";
}

export function getDownloadURLByDatasetId(sessionId, datasetIds) {
    return IDS.server + "/getData?sessionId=" + sessionId + "&datasetIds=" + datasetIds;
}

export function getDownloadURLByDatafileId(sessionId, datafileIds) {
    return IDS.server + "/getData?sessionId=" + sessionId + "&datafileIds=" + datafileIds;
}

export function getFilesByDatasetId(sessionId, datasetIds) {
    return ICATPLUS.server + "/catalogue/sessionId/dataset/id/datasetIds/datafile"
        .replace("sessionId", sessionId)
        .replace("datasetIds", datasetIds);
}

export function getUsersByInvestigationIds(sessionId, investigationIds) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation/id/investigationIds/investigationusers"
        .replace("sessionId", sessionId)
        .replace("investigationIds", investigationIds);
}

export function getInvestigationById(sessionId, investigationId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation/id/investigationId/investigation"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

export function getEmbargoedInvestigations(sessionId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation/status/embargoed/investigation"
        .replace("sessionId", sessionId);
}

export function getReleasedInvestigations(sessionId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation/status/released/investigation"
        .replace("sessionId", sessionId);
}

export function getInvestigationsByUser(sessionId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation?useris=participant"
        .replace("sessionId", sessionId);
}


export function getInvestigationsByInstrumentScientist(sessionId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation?useris=instrumentscientist"
        .replace("sessionId", sessionId);
}


export function getDatasetsById(sessionId, datasetIds) {
    return ICATPLUS.server + "/catalogue/sessionId/dataset/id/datasetIds/dataset"
        .replace("sessionId", sessionId)
        .replace("datasetIds", datasetIds);
}

export function getDatasetByDOI(sessionId, doi) {
    return ICATPLUS.server + "/doi/" + doi + "/datasets?sessionId=" + sessionId;
}

export function getDatasetsByInvestigationId(sessionId, investigationId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation/id/investigationId/dataset"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

export function getDatasetStatus(sessionId, datasetIds) {
    return ICATPLUS.server + "/catalogue/sessionId/dataset/id/datasetIds/status"
        .replace("sessionId", sessionId)
        .replace("datasetIds", datasetIds);
}

export function getDataCollections(sessionId) {
    return ICATPLUS.server + "/catalogue/sessionId/datacollection"
        .replace("sessionId", sessionId);
}

export function getDataCollectionByDatasetId(sessionId, datasetId) {
    return ICATPLUS.server + "/datasets/" + datasetId + "/datacollection?sessionId=" + sessionId;
}

export function createEventFromBase64(sessionId, investigationId) {
    return ICATPLUS.server + "/logbook/sessionId/investigation/id/investigationId/event/createfrombase64"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}

export function getInvestigationUsersByInvestigationId(sessionId, investigationId) {
    return ICATPLUS.server + "/catalogue/sessionId/investigation/id/investigationId/investigationusers"
        .replace("sessionId", sessionId)
        .replace("investigationId", investigationId);
}


/** To be removed from here */
var IDS =
{
    //server: "https://ovm-icat-test.esrf.fr:8181",
    server: "https://ids.esrf.fr/ids",
    connection: {
        plugins: ['esrf']
    }
};