
import { 
  ADD_DATASET_BY_ID, 
  REMOVE_DATASET_BY_ID,
  ADD_INVESTIGATION_BY_ID,
  REMOVE_INVESTIGATION_BY_ID
} from '../constants/ActionTypes'




export function addInvestigationById(investigationId) {  
    return function (dispatch) {
        dispatch(
        {
            type: ADD_INVESTIGATION_BY_ID,
            payload : investigationId
        
        });
    }
}

export function removeInvestigationById(investigationId) {  
    return function (dispatch) {
        dispatch(
        {
            type: REMOVE_INVESTIGATION_BY_ID,
            payload : investigationId
        
        });
    }
}
export function removeDatasetById(datasetId) {            
  return function (dispatch) {      
        dispatch(
        {
            type: REMOVE_DATASET_BY_ID,
            payload : datasetId
        
        });  
    }
}


export function addDatasetById(datasetId) {        
  return function (dispatch) {      
        dispatch(
        {
            type: ADD_DATASET_BY_ID,
            payload : datasetId
        
        });  
    }
}




