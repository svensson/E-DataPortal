import { SET_AVAILABLE_TAGS, CLEAR_AVAILABLE_TAGS, SET_LOGBOOK_CONTEXT } from '../constants/ActionTypes';

/**
 * Action used to set available tags
 * @param {array} availableTags available tags
 * @return {object} action
 */
export function setAvailableTagAction(availableTags) {
    return {
        type: SET_AVAILABLE_TAGS,
        payload: availableTags
    };
}

export function clearAvailableTagAction() {
    return {
        type: CLEAR_AVAILABLE_TAGS,
    };
}

/**
 * Set the logbook context ie the context in which the logbook is used. Here the context corresponds to :
 * - 'proposal': indicating that the logbook is associated to a proposal
 * - 'dataCollection': indicating that the logbook is associated to a dataCollection
 * - 'dataset': indicating that the logbook is associated to a dataset
 * @param {*} context the context object for which the logbook is run.
 */
export function setLogbookContextAction(context) {
    return {
        type: SET_LOGBOOK_CONTEXT,
        context: context,
    };
}