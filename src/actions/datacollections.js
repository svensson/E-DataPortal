import axios from "axios";
import { getDataCollections } from '../api/icat/icatPlus.js';
import { FECTH_DATACOLLECTIONS } from '../constants/ActionTypes';

export function fetchDataCollections(sessionId) {
    return {
        type: FECTH_DATACOLLECTIONS,
        payload: axios.get(getDataCollections(sessionId))

    };
}



