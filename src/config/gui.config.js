/** 
 * gui.config.js 
 * GUI configuration file
 */
export function GUI_CONFIG() {
    return {
        /** Number of logbook events to display per page. EVENTS_PER_PAGE should be lower than EVENTS_PER_DOWNLOAD */
        EVENTS_PER_PAGE: 3000,

        /** Maximum number of logbook events downloaded from the server. This enables to store a larger set of 
        events than those required for a single page thus reducing HTTP requests to the server. */
        EVENTS_PER_DOWNLOAD: 6000,

        /** Default sorting filter for logbook events.
        The format:
           { nameOftheMongoDBFieldToSortAgainst : theSortingOrder } 
        
        Possible values for **sortingOrder**:
           1, for an ascending sorting of nameOftheMongoDBFieldToSortAgainst
          -1, for a descending sorting of nameOftheMongoDBFieldToSortAgainst
        
        */
        DEFAULT_SORTING_FILTER: {
            createdAt: -1
        },

        /** Tinymce editor minimum height when there is a single editor in the modal */
        SINGLE_EDITOR_MIN_HEIGHT: "200px",
        /** Tinymce editor maximum height when there is a single editor in the modal */
        SINGLE_EDITOR_MAX_HEIGHT: "808px",
        /** Tinymce editor minimum height when there are 2 editors in the modal */
        DUAL_EDITOR_MIN_HEIGHT: "270px",
        /** Tinymce editor maximum height when there are 2 editors in the modal */
        DUAL_EDITOR_MAX_HEIGHT: "270px",

        /* Default tag color used in the logbook when tag color is not set */
        DEFAULT_TAG_COLOR: "#a6bded",
        /* Activate the periodic refresh. So periodic http requests are performed*/
        AUTOREFRESH_ENABLED: true,
        /* Whether event list refreshed automatically or not by default. */
        AUTOREFRESH_EVENTLIST: false,
        /* the delay between 2 refreshments of event list. (in milliseconds)*/
        AUTOREFRESH_DELAY: 30000,
    }
}