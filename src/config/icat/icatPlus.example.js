/**
 * icatPlus.example.js configuration file
 */
var ICATPLUS =
{
    /** URL of ICAT+ server */
    server: "http://lindemaria.esrf.fr:8000",
};

export default ICATPLUS;