import 'tinymce/tinymce';
import 'tinymce/themes/modern/theme';
import 'tinymce/plugins/link';
import 'tinymce/plugins/image';
import 'tinymce/plugins/emoticons';
import 'tinymce/plugins/textcolor';
import 'tinymce/plugins/fullscreen';
import 'tinymce/plugins/media';
import 'tinymce/plugins/paste';
import 'tinymce/plugins/table';
import 'tinymce/plugins/lists';
import 'tinymce/plugins/autoresize';
import 'tinymce/plugins/importcss';
import { GUI_CONFIG } from '../gui.config';

// Configuration file for tinyMCE editor

/** the general configuration set used for all instances of the editor */
class GeneralConfig {
    constructor(userView) {
        this.plugins = 'link, image, textcolor, fullscreen, media, paste, table, lists, importcss';
        this.skin_url = '/tinymce/skins/customLightgray';
        this.statusbar = false;
        this.branding = false; // hides powered by tinymce
        this.paste_data_images = true;
        this.content_css = '/tinymce/customStyles.css';
        this.formats = {
            alignleft: { selector: 'img', styles: { float: 'left', margin: '10px' } },
            alignright: { selector: 'img', styles: { float: 'right', margin: '10px' } },
            bullist: { selector: 'ul', styles: { display: 'inline-block' } },
        }
    }
}

/** the configuration set when the editor is used for editing */
export class EditionModeConfig extends GeneralConfig {
    constructor() {
        super();
        this.readonly = false;
        this.menubar = false;
        this.toolbar = 'undo redo paste | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor emoticons | link image media table | fullscreen';
    }
}

/** the configuration set when the editor is used for viewing */
export class ViewModeConfig extends GeneralConfig {
    constructor() {
        super();
        this.readonly = true;
        this.menubar = false;
        this.toolbar = false;
        this.content_style = "div {margin: 10px; border: 50px solid red; padding: 3px}";
    }
}