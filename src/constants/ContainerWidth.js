export const ContainerWidth = {
  ExtraSmall: 768,
  Small: 992,
  Medium: 1200,
  Large: 10000  
}

export const ContainerSize = {
  ExtraSmall: 'xs',
  Small: 'sm',
  Medium: 'md',
  Large: 'lg'
}

