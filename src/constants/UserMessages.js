/* the type of an error message */
export const ERROR_MESSAGE_TYPE = "error";

/* the type of an informative message */
export const INFO_MESSAGE_TYPE = "info";

/* the type of a messsage denoting a successfull operation */
export const SUCCESS_MESSAGE_TYPE = "success";