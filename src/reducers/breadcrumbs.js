import {
  SET_BREADCRUMBS,
  APPEND_BREADCRUMB,
  CLEAN_BREADCRUMBS
} from '../constants/ActionTypes'

import {  
  LOG_OUT
} from '../constants/ActionTypes'


/**
 * Example: "items": [{name:'test', link:"test"}]
 */
const initialState =  {"items": []}

const breadcrumbsList = (state = initialState, action) => {  

  switch (action.type) {
    case SET_BREADCRUMBS: {               
        state = {...state,  items: action.breadcrumbs};                  
        break;
    }
    case APPEND_BREADCRUMB: {                                  
        state = {...state,  items: state.items.concat(action.breadcrumb)};                  
        break;
    }
    case CLEAN_BREADCRUMBS:{      
        state = {items:[]}                  
        break;  
    }   
    case LOG_OUT:{
        state = {items:[]}          
        break;  
    } 
    default:
        break;
  }
  return state;
}

export default breadcrumbsList;


