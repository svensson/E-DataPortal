import { SET_AVAILABLE_TAGS, CLEAR_AVAILABLE_TAGS, SET_LOGBOOK_CONTEXT } from '../constants/ActionTypes';
import { FETCHING_STATUS, FETCHED_STATUS } from '../constants/EventTypes';

// initialize the logbook redux state
const initialState = {
    /* all available tags for that investigation */
    availableTags: [],
    availableTagsReceptionStatus: FETCHING_STATUS,
    context: {
        name: null,
        isReleased: null
    }
};

/**
 *  The logbook reducer. THe reducer takes the current state and the action and returns the new state 
 */
const logbookReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_AVAILABLE_TAGS: {
            state = {
                ...state,
                availableTags: action.payload,
                availableTagsReceptionStatus: FETCHED_STATUS
            };
            break;
        }

        case CLEAR_AVAILABLE_TAGS: {
            state = {
                ...state,
                availableTags: [],
                availableTagsReceptionStatus: FETCHING_STATUS
            };
            break;
        }

        case SET_LOGBOOK_CONTEXT: {
            state = {
                ...state,
                context: {
                    name: action.context.name,
                    isReleased: action.context.isReleased
                }
            }
            break;
        }
        
        default: break; // leave the state unchanged when the action should have no impact on current logbook store state
    };

    return state;
};

export default logbookReducer;
