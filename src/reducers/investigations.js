import {
  FECTH_INVESTIGATIONS_FULFILLED, 
  FECTH_INVESTIGATIONS_PENDING,
  LOG_OUT
} from '../constants/ActionTypes'


const initialState =  {"fetching": false, "fetched": false, "data":[], "error": null}

const investigations = (state = initialState, action) => {      
      
  switch (action.type) {
      
    case FECTH_INVESTIGATIONS_PENDING:{              
        state = {...state,  fetched: false, fetching: true}                
        break;
    }
    case FECTH_INVESTIGATIONS_FULFILLED: {        
       
        state = {...state, data: action.payload.data.map((object,i ) => object.Investigation), fetched: true, fetching: false}                
        break;
    }  
    
   
  
    
    case LOG_OUT: {        
        state = {...state, "fetching": false, "fetched": false, data: []};   
        break;  
    }
    default:
        break;
  }
  return state;
}

export default investigations;


