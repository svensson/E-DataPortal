import {
    ADD_DATASET_BY_ID,
    REMOVE_DATASET_BY_ID,
    ADD_INVESTIGATION_BY_ID,
    REMOVE_INVESTIGATION_BY_ID
} from '../constants/ActionTypes'


const initialState = { "datasetIds": [], "investigationIds": [], "datafileIds": [] }

const selection = (state = initialState, action) => {

    var datasetIds = [];
    switch (action.type) {
        case ADD_INVESTIGATION_BY_ID: {
            break;
        }

        case REMOVE_INVESTIGATION_BY_ID: {
            break;
        }
        case ADD_DATASET_BY_ID: {
            datasetIds = state.datasetIds;
            datasetIds.push(action.payload);
            state = { ...state, datasetIds: datasetIds }
            break;
        }
        case REMOVE_DATASET_BY_ID: {
            datasetIds = state.datasetIds.filter(function (o) { return o !== action.payload })
            state = { ...state, datasetIds: datasetIds }
            break;
        }
        default:
            break;
    }
    return state
}

export default selection
