import { combineReducers } from 'redux';
import user from './login';
import myInvestigations from './myInvestigations.js';
import datasets from './datasets.js';
import logbook from './logbook';
import selection from './selection.js';
import breadcrumbsList from './breadcrumbs.js';
import investigations from './investigations.js';
import datacollections from './datacollections.js';
import releasedInvestigations from './releasedInvestigations.js';
import scientistInstrumentInvestigations from './scientistInstrumentInvestigations.js';

export default combineReducers({
    user,
    myInvestigations,
    investigations,
    releasedInvestigations,
    datasets,
    logbook,
    selection,
    breadcrumbsList,
    datacollections,
    scientistInstrumentInvestigations
});