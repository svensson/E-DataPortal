import React from 'react';
import './DOIBadge.css';
import { Glyphicon} from 'react-bootstrap';


export class DOIBadge extends React.Component {

    render() {
        if (this.props.doi != null) {
            if (this.props.doi.trim() != ""){
                let doiUrl = "https://doi.esrf.fr/" + this.props.doi;
                return <span className='doiBadge borderRadius-5 '>DOI<a id='doiLink' target='_blank' rel='noopener noreferrer' href={doiUrl} >{this.props.doi}</a></span>;
            }
            return null;
        }
        return  <Glyphicon glyph="lock" />

    }
}

export default DOIBadge;
