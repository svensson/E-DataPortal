import React from 'react';
import PropTypes from 'prop-types';

import { Glyphicon } from 'react-bootstrap';

class NewlyAvailableEventsDialogue extends React.Component {
    render() {
        if (this.props.isEventListAutorefreshEnabled === true) {
            return <div className='btn btn-sm' style={{ color: '#888888' }}> Auto refreshing ...</div>
        }
        else {
            let { eventCountSinceLastRefresh, onIconClicked, latestEvents } = this.props;
            let newEventCountSinceLastRefresh = 0;
            if (latestEvents && latestEvents.data && eventCountSinceLastRefresh != undefined) {
                newEventCountSinceLastRefresh = (latestEvents.data.length < eventCountSinceLastRefresh) ? 0 : latestEvents.data.length - eventCountSinceLastRefresh
            }
            if (newEventCountSinceLastRefresh > 0) {
                return (<div className='btn btn-sm' style={{ color: '#888888' }}> {newEventCountSinceLastRefresh
                } new log(s) arrived. <Glyphicon onClick={onIconClicked} glyph='refresh' /> </div>)
            }

            return null;
        }
    }
}

export default NewlyAvailableEventsDialogue;

NewlyAvailableEventsDialogue.proptype = {
    /* Total number of event in the logbook known by the client since the last refresh of the event list */
    eventCountSinceLastRefresh: PropTypes.number,
    /* Callback function triggered which the user clicks the icon*/
    onIconClicked: PropTypes.func,
    /* Latest events of the logbook. Array is in latestEvents.data */
    latestEvents: PropTypes.object
}