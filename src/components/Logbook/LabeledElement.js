import React from 'react';
import Proptypes from 'prop-types';
import { InputGroup, Label, FormControl, OverlayTrigger, Tooltip } from 'react-bootstrap';

/**
 * A React component which renders a labeled elemtn (input or simple text)
 */
class LabeledElement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: props.data || ''
        };
    }

    render() {
        let { data, hasNoData, labelText, setTitleInput, tooltip, type } = this.props;

        if (type === 'input') {
            return (
                <OverlayTrigger
                    placement='top'
                    overlay={
                        <Tooltip id='tooltip'> {tooltip} </Tooltip>
                    }>


                    <InputGroup>
                        <InputGroup.Addon style={{ backgroundColor: 'transparent', border: 'none' }}>
                            <Label> {labelText} </Label>
                        </InputGroup.Addon>
                        <FormControl
                            type='text'
                            value={this.state.inputValue}
                            onChange={this.onChangeInputValue}
                            placeholder={'Optional ' + labelText + ' here'}
                            ref={setTitleInput}
                        />
                    </InputGroup>
                </ OverlayTrigger>

            );
        }

        if (type === 'text') {
            return (<Overlay {...this.props}>
                <div>
                    <Label> {labelText} </Label>
                    {(hasNoData === true || (hasNoData === false && data)) ?
                        <div style={{ paddingTop: '5px', paddingLeft: '10px', color: '#666666', display: 'inline-block' }}> {data} </div>
                    :
                        <div style={{ paddingTop: '5px', paddingLeft: '10px', color: '#888888', display: 'inline-block', fontStyle: 'italic' }}> Not available </div>
                    }
                </div>
            </Overlay>);
        }
    }

    componentDidUpdate() {
        if (this.props.type === 'input') { this.props.onEventModified(); };
    }

    onChangeInputValue(e) {
        this.setState({ inputValue: e.target.value });
    }
};

export default LabeledElement;

LabeledElement.proptypes = {
    /** data to render */
    data: Proptypes.string,
    /** whehther data are provided or not */
    hasNoData: Proptypes.bool.isRequired,
    /** Text of the label */
    labelText: Proptypes.string,
    /** callback function triggered when user changed the data of the event */
    onEventModified: Proptypes.func,
    /** Callback function used to get data from this input from the parent compoenent */
    setTitleInput: Proptypes.func,
    /** When defined, this component has an overlay to display the provided tooltip. No overlay when this prop is not set. */
    tooltip: Proptypes.object,
    /** type of the element to render : 'input' or  */
    type: Proptypes.string.isRequired,
}

/** Simple react component which render an overlay or not */
const Overlay = (props) => {
    if (props.tooltip) {
        return (<OverlayTrigger
            placement='top'
            overlay={<Tooltip id='tooltip'> {props.tooltip} </Tooltip>}>
            {props.children}
        </OverlayTrigger>);
    };
    return props.children // no tooltic by default
}