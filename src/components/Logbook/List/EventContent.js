import React from 'react';
import PropTypes from 'prop-types';
import { getOriginalEvent, getEventIcon, getLastCommentContent, getContent } from '../../../helpers/EventHelpers'
import EventContentDisplayer from '../EventContentDisplayer';
import { ANNOTATION, NOTIFICATION, DOC_VIEW } from '../../../constants/EventTypes';



/**
 * A basic event of the logbook
 */
class ContentEvent extends React.Component {
    render() {

        let { event, toggleMode, view, investigationId } = this.props;
        let paddingTopDate = (event.type === ANNOTATION) ? '6px' : '0px';

        if (event.content && (getContent(event.content, 'plainText') !== null || getContent(event.content, 'html') !== null)) {
            if (event.type === ANNOTATION) {
                return <AnnotationContent event={event} view={view} />
            } else if (event.type === NOTIFICATION) {
                return <NotificationContent event={event} />
            }
        } 
        return <div>
            <div style={{ flexGrow: '1', marginLeft: '10px', paddingTop: '4px', paddingBottom: '4px' }} >
                <Icon event={event} view={view} />
            </div>

            <p style={{ margin: '0px', color: '#777777', fontStyle: 'italic' }}>This event has no content</p>
        </div>;
            
        
        
    }
}

ContentEvent.propTypes = {
    /* the event object as received from the ICAT+ server */
    event: PropTypes.object.isRequired,
    /** whether this event is selected or not */
    isSelected: PropTypes.bool,
    /* callback functon which trigger a change to another mode */
    toggleMode: PropTypes.func.isRequired,
    /** the user who is currently logged in */
    user: PropTypes.object.isRequired,
    /* the current view */
    view: PropTypes.string.isRequired,
}


const AnnotationContent = (props) => {
    let { event, view } = props;

    return (
        <div style={{ flexGrow: '1', marginLeft: '10px', paddingTop: '4px', paddingBottom: '4px' }} >
            <Icon event={event} view={view} />

            <EventContentDisplayer
                content={event.content}
                useRichTextEditor={false}
                isEditionMode={false}
            />
        </div>
    )
}

const NotificationContent = (props) => {
    let { event } = props;

    let notificationMessage = getOriginalEvent(event);
    let renderedCommentContent;
    let lastCommentContent = getLastCommentContent(event);
    if (lastCommentContent) {
        renderedCommentContent = () => (
            <div style={{ clear: 'left', marginLeft: '30px', paddingTop: '8px', paddingBottom: '8px' }}>
                {/* <div className='pull-left' style={{ paddingRight: '8px' }}>
                    {getEventIcon('comment', "20")}
                </div> */}
                <EventContentDisplayer
                    content={lastCommentContent}
                    useRichTextEditor={false}
                    isEditionMode={false}
                />
            </div>)
    } else { renderedCommentContent = () => null }

    return (
        <div style={{ flexGrow: '1', marginLeft: '10px' }} >
            <div className='pull-left' style={{ paddingRight: '8px' }}>
                {getEventIcon(event.category, "20")}
            </div>

            <div id='divContainingHTMLNotificationInDocView'>
                <EventContentDisplayer
                    content={notificationMessage.content}
                    useRichTextEditor={false}
                    isEditionMode={false}
                />
            </div>

            {renderedCommentContent()}
        </div>)
}

const Icon = (props) => {
    let { event, view } = props;
    if (view && view === DOC_VIEW) {
        return null
    } 

    return (<div className='pull-left' style={{ paddingRight: '8px' }}>
        {getEventIcon(event.category, "20")}
    </div>
    )
}


export default ContentEvent;