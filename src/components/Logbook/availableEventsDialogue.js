import React from 'react';
import OverlayBox from '../OverlayBox';
class availableEventsDialogue extends React.Component {
    render() {
        return (<span style={{ color: '#888888' }}>
            {this.props.newlyArrivedEventCount ? this.props.newlyArrivedEventCount : 0} new logs available.
            </span>)
    }
}

export default availableEventsDialogue;