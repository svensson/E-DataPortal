import React from 'react'
import PropTypes from 'prop-types';
import { Grid, Row, Col, Panel } from 'react-bootstrap'

import EventHeader from './EventHeader'
import EventContentDisplayer from './EventContentDisplayer';
import { ORIGINAL_EVENT_VERSION, LATEST_EVENT_VERSION, MIDDLE_EVENT_VERSION } from '../../constants/EventTypes';
import { getEventHistoryCreationDate, getTags } from '../../helpers/EventHelpers';
import TagListInLine from './Tag/TagListInLine';

/*
* React component which renders a panel showing the provided event details using version context provided by the version property.
*/
class EventVersionPanel extends React.Component {

    constructor(props) {
        super(props);

        this.getEventHeaderTextByVersion = this.getEventHeaderTextByVersion.bind(this);
        this.getEventFooterTextByVersion = this.getEventFooterTextByVersion.bind(this);
    }
    render() {
        let { availableTags, event, version } = this.props;
        return (
            <div class='margin-bottom-10'>
                <Panel>
                    <EventHeader text={this.getEventHeaderTextByVersion(event, version)} />
                    <Panel.Body>
                        <Row>
                            <Col xs={12}>
                                <EventContentDisplayer content={event.content} useRichTextEditor={false} isEditionMode={false} />
                            </Col>
                        </Row>
                    </Panel.Body>
                    <Panel.Footer>
                        <Grid>
                            <Row>
                                <Col xs={12} md={6}>
                                    <span className='padding-left-5' style={{ fontStyle: 'italic' }}>
                                        {this.getEventFooterTextByVersion(event, version)}
                                    </span>
                                </Col >
                                <Col xs={12} md={6}>
                                    <TagListInLine tags={getTags(event, availableTags)} />
                                </Col>
                            </Row>
                        </Grid>
                    </Panel.Footer>
                </Panel>
            </div>)
    }

    getEventHeaderTextByVersion(event, version) {
        if (event && version) {
            if (version === ORIGINAL_EVENT_VERSION) {
                return 'On ' + getEventHistoryCreationDate(event) + ', the original message was: ';
            }
            if (version === LATEST_EVENT_VERSION) {
                return 'Written on ' + getEventHistoryCreationDate(event) + ', the latest version of the comment is:'
            }
            if (version === MIDDLE_EVENT_VERSION) {
                return 'Written on ' + getEventHistoryCreationDate(event) + ', the comment was:'
            }
        } else return '';
    }

    getEventFooterTextByVersion(event, version) {
        if (event && version) {
            if (version === ORIGINAL_EVENT_VERSION) {
                return 'Created by ' + event.username;
            }
            if (version === LATEST_EVENT_VERSION || MIDDLE_EVENT_VERSION) {
                return 'Commented by ' + event.username;
            }
        } else { return '' };
    }
}

EventVersionPanel.propTypes = {
    /** the event */
    event: PropTypes.object,
    /** event version */
    version: PropTypes.string
}

export default EventVersionPanel