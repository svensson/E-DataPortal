import React from "react";
import { Pagination } from 'react-bootstrap'
import PropTypes from 'prop-types';
import { GUI_CONFIG } from "../../config/gui.config";


/* This class creates the page component */
class EventPager extends React.Component {
    constructor(props) {
        super(props);
        this.getTotalPageNumber = this.getTotalPageNumber.bind(this);
    }

    render() {

        function getStyle(isFloatingRight) {
            let style = {};
            if (isFloatingRight && isFloatingRight === true) {
                style.float = 'right'
            } else {
                style.textAlign = 'right'
                style.marginBottom = "55px"
            }


            style.marginRight = '40px';
            return style;
        }


        let paginationItemArray = [];
        let totalPages = this.getTotalPageNumber();
        if (totalPages <= 1){
            return null;
        }
        
        for (let number = 1; number <= totalPages; number++) {
            paginationItemArray.push(
                <Pagination.Item key={number} id={number} onClick={() => (this.props.onPageClicked(number))} active={number === this.props.activePage}>{number}</Pagination.Item>
            );
        }

        if (this.props.eventCount === 0) {
            return null;
        }

        return (
            <div style={getStyle(this.props.isFloatingRight)}>
                <Pagination bsSize="medium" style={{ marginTop: '5px', marginBottom: '5px' }}>
                    {/* <Pagination.First id="0" onClick={onPageNumberClicked} />
                    <Pagination.Prev id={(this.activePage - 1 < 0 ? 0 : this.activePage - 1)} onClick={onPageNumberClicked} /> */}
                    {paginationItemArray}
                    {/* <Pagination.Next id={(this.activePage + 1 >= totalPages ? totalPages - 1 : this.activePage + 1)} onClick={onPageNumberClicked} />
                    <Pagination.Last id={totalPages - 1} onClick={onPageNumberClicked} /> */}
                </Pagination>
            </div>

        )
    }

    /**
    * Get the number of pages requested to display the data
    */
    getTotalPageNumber() {
        if (GUI_CONFIG().EVENTS_PER_PAGE) {
            if (this.props.eventCount < GUI_CONFIG().EVENTS_PER_PAGE) {
                return 1;
            } else {
                return (Math.ceil(this.props.eventCount / GUI_CONFIG().EVENTS_PER_PAGE));
            }
        }
        // if in case of error, displays all items on the same page
        console.log("[ERROR] Could not get the number of events per page. CHeck the config file.")
        return 1;
    }

}

EventPager.proptypes = {
    /* the page which is currently displayed */
    activePage: PropTypes.number.isRequired,
    /* the total number of events found by the server for a given search */
    eventCount: PropTypes.number.isRequired,
    /* whether the page is flaoting on the right side */
    isFloatingRight: PropTypes.bool

}

export default EventPager;