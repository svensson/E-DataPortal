import React from 'react';
import PropTypes from 'prop-types';
import { Panel } from 'react-bootstrap';
import EventHeader from './EventHeader';
import EventFooter from './EventFooter';
import { ORIGINAL_EVENT_VERSION, LATEST_EVENT_VERSION, PUBLIC_EVENT_DETAILS_INVISIBLE, ANNOTATION } from '../../constants/EventTypes';
import { getOriginalEvent, getPreviousVersionNumber } from '../../helpers/EventHelpers';
import _ from 'lodash';
import EventVersionPanel from './EventVersionPanel';

/**
 * React component which renders a public event. It is used only when the logbook context has become public
 */
class PublicEventHistoryPanel extends React.Component {
    render() {
        let { availableTags, event } = this.props;

        return (
            <Panel bsStyle='primary' style={{ marginBottom: '0px', height: '100%', position: 'relative' }}>
                <EventHeader text={'Public log details'} />
                <div style={{ position: 'absolute', top: '41px', bottom: '42px', width: '100%', overflowY: 'scroll', overflowX: 'hidden' }} >
                    <div style={{ padding: '10px' }}>
                        {/* try to show latest version of the event */}
                        {getPreviousVersionNumber(event) === 0 ?
                            <p style={{ fontStyle: 'italic', color: '888888', paddingBottom: '10px' }}>This event has never been commented.</p>
                            : null}

                        {/* Always show the latest version*/}
                        <EventVersionPanel
                            availableTags={availableTags}
                            event={event}
                            version={LATEST_EVENT_VERSION} />

                        {/* Show the original version if it exists for all except annotations */}
                        {!_.isEqual(event.type, ANNOTATION) && getPreviousVersionNumber(event) !== 0 ?
                            <EventVersionPanel
                                availableTags={availableTags}
                                event={getOriginalEvent(event)}
                                version={ORIGINAL_EVENT_VERSION} />
                            : null}

                    </div>

                </div>
                <div style={{ position: 'absolute', bottom: '0px', width: '100%' }}>
                    <EventFooter
                        isSaveButtonEnabled={false}
                        isSaveButtonVisible={false}
                        onCancelButtonClicked={() => this.onCancelButtonClicked()}
                        cancelButtonLabel='Close' />
                </div>
            </Panel >
        )
    }

    /**
     * Callback function triggered when the user clicks on the cancel button
     */
    onCancelButtonClicked() {
        this.props.setPublicEventVisibility(PUBLIC_EVENT_DETAILS_INVISIBLE);
    }
}

PublicEventHistoryPanel.propTypes = {
    /* List of availalbe tags*/
    availableTags: PropTypes.array,
    /* event to edit. Null when a new event is beaing created. */
    event: PropTypes.object,
    /* callback function used to change the visibility of the panel */
    setPublicEventVisibility: PropTypes.func,
}

export default PublicEventHistoryPanel;