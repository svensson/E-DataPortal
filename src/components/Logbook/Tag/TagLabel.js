import React from 'react'
import PropTypes from 'prop-types';
import { Label, Glyphicon } from 'react-bootstrap';

/** React component which renders a tag label */
class TagLabel extends React.Component {
    render() {
        let { tag, showDeleteButton } = this.props;

        return (<div style={{ display: 'inline-block', marginRight: '4px' }}>
            <Label
                style={{
                    backgroundColor: tag.color,
                    borderBottomLeftRadius: '10px',
                    borderBottomRightRadius: showDeleteButton === true ? '0px' : '10px',
                    borderTopLeftRadius: '10px',
                    borderTopRightRadius: showDeleteButton === true ? '0px' : '10px',
                    display: 'inline-block',
                    marginBottom: '2px'
                }}>
                {tag.name}
            </Label>

            {showDeleteButton === true ?
                <Label
                    style={{
                        backgroundColor: '#777777',
                        borderTopLeftRadius: '0px',
                        borderTopRightRadius: '10px',
                        borderBottomRightRadius: '10px',
                        borderBottomLeftRadius: '0px',
                        display: 'inline-block',
                        marginBottom: '2px',
                        paddingLeft: '0px',
                        paddingRight: '1px'
                    }}>
                    <Glyphicon
                        glyph="remove"
                        bsSize="xsmall"
                        style={{
                            backgroundColor: '#777777',
                            color: '#FFFFFF',
                            verticalAlign: 'bottom'
                        }}
                        onClick={() => this.props.onDeleteTagClicked(tag)}
                    />
                </Label> : null}
        </div>)
    }
}

TagLabel.propTypes = {
    /* Callback function used to remove a tag from the selection. Used in EVENT context only */
    onDeleteTagClicked: PropTypes.func,
    /* Whether the delete button is displayed or not */
    showDeleteButton: PropTypes.bool,
    /* the tag to render */
    tag: PropTypes.object.isRequired,
}

TagLabel.defaultProps = {
    showDeleteButton: false,
}

export default TagLabel;