import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Row, Col, FormControl, Popover, OverlayTrigger, Button, Panel, FormGroup, ControlLabel } from 'react-bootstrap';
import { GUI_CONFIG } from '../../../config/gui.config';
import { GithubPicker } from 'react-color';
import _ from 'lodash';


/**
 * React component used to edit or create a tag
 */
class NewOrEditTagPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tag: _.cloneDeep(this.props.tag) || { name: '', description: '', color: GUI_CONFIG().DEFAULT_TAG_COLOR, investigationId: this.props.investigationId },
        };

        this.onChangeComplete = this.onChangeComplete.bind(this);
    }

    render() {
        let tag = this.state.tag;

        return (
            <Grid fluid style={{ marginTop: '20px', marginBottom: '70px' }}>
                <Row className='show-grid'>
                    <Col xs={0} md={1}> </Col>
                    <Col xs={12} md={10}>
                        <Panel bsStyle='primary'>
                            <Panel.Heading> <strong> {this.props.panelHeaderText} </strong> </Panel.Heading>
                            <Panel.Body>
                                <FormGroup controlId='formControlTagLabel'>
                                    <ControlLabel>Label <Mandatory></Mandatory></ControlLabel>
                                    <FormControl
                                        componentClass='input'
                                        type='text'
                                        placeholder='Tag name'
                                        value={tag.name}
                                        onChange={(e) => this.onInputChange('name', e)} />
                                </FormGroup>

                                <FormGroup controlId='formControlTagDescription'>
                                    <ControlLabel>Description </ControlLabel>
                                    <FormControl
                                        componentClass='input'
                                        type='text'
                                        placeholder='Tag description'
                                        value={tag.description}
                                        onChange={(e) => this.onInputChange('description', e)} />
                                </FormGroup>

                                <FormGroup controlId='formControlTagScope'>
                                    <ControlLabel>Scope <Mandatory></Mandatory> </ControlLabel>
                                    <FormControl
                                        componentClass='select'
                                        type='text'
                                        placeholder='Tag scope'
                                        value={tag.description}
                                        onChange={(e) => this.onInputChange('scope', e)}
                                    >
                                        <option value='investigation'>investigation</option>
                                        {/* <option value='beamline'>beamline</option> */}
                                    </FormControl>
                                </FormGroup>

                                <FormGroup controlId='formControlTagColor'>
                                    <ControlLabel> Color </ControlLabel>
                                    <Row>
                                        <Col xs='12'>
                                            <OverlayTrigger
                                                trigger='click'
                                                placement='top'
                                                rootClose='true'
                                                overlay={PopoverColorPickerBottom(this.onChangeComplete)}
                                            >
                                                <div
                                                    style={{
                                                        backgroundColor: tag.color || GUI_CONFIG().DEFAULT_TAG_COLOR,
                                                        width: '30px',
                                                        height: '30px',
                                                        marginLeft: '50%',
                                                        marginRight: '50%'
                                                    }}>
                                                </div>
                                            </OverlayTrigger>
                                        </Col >
                                    </Row>
                                </FormGroup>
                                <br />

                                <div style={{ float: 'right' }}>
                                    <Button bsStyle='primary' style={{ marginRight: '10px' }} onClick={() => this.onSave()}> Save </Button>
                                    <Button bsStyle='primary' style={{ marginRight: '10px' }} onClick={() => this.props.showAvailableTags()} > Cancel </Button>
                                </div>
                            </Panel.Body>
                        </Panel>
                    </Col>
                    <Col xs={0} md={1}> </Col>

                </Row>
            </Grid>
        );
    }

    /** 
     * CallBack function triggered when the user changes the tag field
    */
    onInputChange(changedTagProperty, event) {
        let value = event.target.value;

        let tagCopy = this.state.tag;
        tagCopy[changedTagProperty] = value;

        this.setState({
            tag: tagCopy
        });
    }

    /* CallBack function triggered when the user selects a new color */
    onChangeComplete(color) {
        let fakeEvent = {};
        fakeEvent.target = {};
        fakeEvent.target.value = color.hex;
        this.onInputChange('color', fakeEvent);
    }

    onSave() {
        if (this.props.tag) {
            this.props.updateTags([this.state.tag]);
        } else {
            this.props.createNewTag(this.state.tag);
        };
    }
}

class Mandatory extends React.Component {
    render() {
        return (<span style={{ color: 'red' }}>*</span>);
    }
};

const PopoverColorPickerBottom = (callback) => {
    return (<Popover id='popover-positioned-bottom' title='Select a color'>
        <GithubPicker triangle='hide' width='230px' onChangeComplete={callback} />
    </Popover>
    );
};

NewOrEditTagPanel.propTypes = {
    /** Text display in the panel header */
    panelHeaderText: PropTypes.string,
    /** callback function used to show the tag list */
    showAvailableTags: PropTypes.func,
    /* if present, it corresponds to the tag to edit. If absent, it indcates that a tag is being created*/
    tag: PropTypes.object
};

export default NewOrEditTagPanel;