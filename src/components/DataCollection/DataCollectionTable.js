import React from 'react';
import { Button, Grid, Row, Col,Label, Glyphicon } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import DOIBadge from '../../components/doi/DOIBadge.js';
import _ from 'lodash';
import { TITLE, INVESTIGATION_NAMES, INSTRUMENT_NAMES } from '../../constants/ParameterTypes.js';
import ResponsiveTable from "../Table/ResponsiveTable.js";

class DataCollectionTable extends React.Component {
 
  doiFormatter(cell, row) {           
     return <DOIBadge doi={cell}></DOIBadge>
  }

  
   titleFormatter(cell, datacollection, extraData) {     
     
     var title = _.find(datacollection.parameters, function(o){ return o.name === TITLE });     
     if (title){
       if (title.value){        
            return (title.value);
       }
     }      
  }

    nameFormatter(cell, datacollection, extraData) {    
       
     var names = _.find(datacollection.parameters, function(o){ return o.name === INVESTIGATION_NAMES });     

     if (names){
       if (names.value){  
            var short = JSON.parse(JSON.stringify(names.value));            
            if (short.length > 13){
                short = short.slice(0,10)+ "...";
            }
         
            return <Link to={`/public/${datacollection.doi}`}>                
                <Button  bsSize="xsmall"  style={{width:120, textAlign:'left'}}>
                  <Glyphicon glyph="circle-arrow-right" />
                  <span style={{marginLeft:'10px'}}>{short} </span>
                </Button>     
          </Link>;
       }
     }      
  }

     beamlineFormatter(cell, datacollection, extraData) {     
       
     var names = _.find(datacollection.parameters, function(o){ return o.name === INSTRUMENT_NAMES });     
     if (names){
       if (names.value){        
            return (names.value);
       }
     }      
  }

    releaseFormatter(cell, datacollection, extraData) {     
        return <Label bsStyle="primary">RELEASED</Label>
    }

    datasetCountFormmater(cell, datacolletion, rowIndex, extraData) {       
      return datacolletion.dataCollectionDatasets.length;
    }

    dataCollectionFormatter(cell, datacolletion, rowIndex, extraData) {      
    return <Grid style={{textAlign:'center'}}>
            <Row className="show-grid">
              <Col xs={12} md={12}>
                {extraData.nameFormatter(cell, datacolletion, extraData)}
              </Col>
             
            </Row>
             <Row className="show-grid">
             <Col xs={12} md={12}>
                {extraData.beamlineFormatter(cell, datacolletion, extraData)}
              </Col>
            </Row>
            <Row className="show-grid">
              <Col xs={12}>
                  <div style={{color:'gray', fontStyle:'italic'}}> {extraData.titleFormatter(cell, datacolletion, extraData)}</div>
              </Col>             
            </Row>           
             <Row className="show-grid" style={{fontSize:'10px'}}>
              <Col xs={12} >
                   <span >{extraData.doiFormatter(datacolletion.doi, datacolletion, extraData)}</span>   
              </Col>             
            </Row>
          </Grid>;
      
  }

 getColumns() {  
     
    return [
        {
        text: "id",
        dataField: "id",
        hidden: true
      },
       {
        text: "Experiment",
        dataField: "name",
        formatter: this.dataCollectionFormatter,
        formatExtraData: this,
        sort: true,
        hidden: false,
        headerStyle: (column, colIndex) => {
          return { width: "50%", textAlign: "center" };
        },
        responsiveHeaderStyle: {
          xs:  { width: "100%"},
          sm:  { width: "100%"},
          md:  { hidden : true },
          lg:  { hidden : true },
        }
      },
       {
        text: "Proposal",
        dataField: "name",
        formatter: this.nameFormatter,
        formatExtraData: this,
        sort: true,
        hidden: false,
        headerStyle: (column, colIndex) => {
          return { width: "50%", textAlign: "center" };
        },
        responsiveHeaderStyle: {
          xs: { hidden : true },
          sm: { hidden : true },
          md: { width: "140px", textAlign: "center" },
          lg: { width: "140px", textAlign: "center" }
        }
      },
      {
        text: "Beamline",
        dataField: "visitId",
        formatter: this.beamlineFormatter,
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden : true },
          sm: { hidden : true },
          md: { width: "140px", textAlign: "center" },
          lg: { width: "140px", textAlign: "center" }
        }
      },
      {
        text: "Title",
        dataField: "summary",
        formatter: this.titleFormatter,        
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden : true},
          sm: { hidden : true },
        
        }
      },  
      {
        text: "Datasets",
        dataField: "summary",
        formatter: this.datasetCountFormmater,        
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden : true},
          sm: { hidden : true },
          md: { width: "100px", textAlign: "center" },
          lg: { width: "100px", textAlign: "center" }
        
        }
      },  
      
       {
        text: "DOI",
        dataField: "doi",
        formatter: this.doiFormatter,
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden : true },
          sm: { hidden : true },
          md: { hidden : true },
          lg: { width: "300px", textAlign: "center" }
        }
      }
    ];
 }
  
  render() {   
    /*if (this.props.fetching) {
      return <Loading message="Loading datacollections"></Loading>;
    }     */
    return (
      <ResponsiveTable        
        keyField="id"
        data={this.props.datacollections}
        columns={this.getColumns()}
      />
    );    
  }
}

export default DataCollectionTable;
