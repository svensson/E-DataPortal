import React from 'react';
import { Badge, Tab, Tabs, Panel } from 'react-bootstrap';
import { getInvestigationUsersByInvestigationId } from '../../api/icat/icatPlus.js';
import axios from 'axios';
import ResponsiveTable from "../Table/ResponsiveTable.js";

class InvestigationWidget extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            investigationUsers : [],
            fetched: false,
            fetching: false            
        }
    }

    componentDidMount() {        
        axios.get(getInvestigationUsersByInvestigationId(this.props.sessionId, this.props.investigation.id))
            .then(res => {                
                this.setState({
                    investigationUsers: res.data,
                    fetching: false,
                    fetched: true,
                });
            })
            .catch((error) => {                
                this.setState({                    
                    fetching: false,
                    fetched: false,
                });
            });
    }

    getColumns() {

        return [
          {
            text: "id",
            dataField: "name",
            hidden: true
          },
          {
            text: "Name",
            dataField: "fullName"            
          },
          {
            text: "Role",
            dataField: "role"        
          }
        ]
    };

    render() {                                
        return <Panel >
            <Panel.Body >
                <Tabs id="tabs">                    
                    <Tab style={{margin:30}} eventKey={1} title="Participants">
                    <ResponsiveTable 
                        keyField="id"
                        data={this.state.investigationUsers}
                        columns={this.getColumns()}                                           
                    />

                    </Tab>                    
                </Tabs>
            </Panel.Body>
            <Panel.Footer>
                
            </Panel.Footer>
        </Panel>

    }

/*
                    <Tab eventKey={12} title="DOI" mountOnEnter={true}>
                        <DatasetDOIList dataset={dataset} sessionId={this.props.sessionId}> ></DatasetDOIList>
                    </Tab>
*/

}

export default InvestigationWidget;


