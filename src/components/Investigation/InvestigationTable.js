import React from "react";
import Moment from "react-moment";
import { Grid, Row, Col, Button, Glyphicon } from "react-bootstrap";
import { Link } from "react-router-dom";
import _ from "lodash";
import Loading from "../../components/Loading.js";
import DOIBadge from "../../components/doi/DOIBadge.js";
import { stringifyBytesSize } from "../Helpers.js";
import ResponsiveTable from "../Table/ResponsiveTable.js";
import InvestigationWidget from '../Investigation/InvestigationWidget.js';

import {
  SAMPLE_COUNT,
  FILE_COUNT,
  VOLUME,
  DATASET_COUNT
} from "../../constants/ParameterTypes.js";

class InvestigationTable extends React.Component {

  doiFormatter(cell, row) {
    return <DOIBadge doi={cell} />;
  }

  dateFormatter(cell, row) {
    if (cell != null) {
      return (
        <Moment parse="YYYY-MM-DD HH:mm" format="DD/MM/YYYY">
          ${cell}
        </Moment>
      );
    }
  }

  beamlineFormatter(cell, row) {
    return <span style={{ fontWeight: 'bold' }}>{cell.toUpperCase()}</span>;
  }

  nameFormatter(cell, investigation, rowIndex, extraData) {    
    if (extraData.props.linkProposal || extraData.props.user.isAdministrator) {
      return (
        <Link to={`/investigation/${investigation.id}/datasets`}>
          <Button bsSize="xsmall" style={{ width: 120, textAlign: 'left' }}><Glyphicon glyph="circle-arrow-right" /><span style={{ marginLeft: '10px' }}>{investigation.name} </span></Button>
        </Link>
      );
    } else {
      return <span style={{ fontWeight: "bold" }}>{investigation.name}</span>;
    }
  }

  logbookFormatter(cell, investigation, rowIndex, extraData) {
    return (
      <Link to={`/investigation/${investigation.id}/events`}>
        <span className="glyphicon glyphicon-list-alt"> </span>
      </Link>
    );
  }

  volumeFormatter(cell, investigation, rowIndex, extraData) {
    var volume = _.find(investigation.parameters, function (o) {
      return o.name === VOLUME;
    });
    if (volume) {
      if (volume.value) {
        if (volume.value !== 0) {
          return stringifyBytesSize(volume.value);
        }
      }
    }
  }

  experimentFormatter(cell, investigation, rowIndex, extraData) {
    return <Grid style={{ textAlign: 'center' }}>
      <Row className="show-grid">
        <Col xs={12} md={12}>
          {extraData.nameFormatter(investigation.name, investigation, rowIndex, extraData)}
        </Col>

      </Row>
      <Row className="show-grid">
        <Col xs={12} md={12}>
          {extraData.beamlineFormatter(investigation.visitId, investigation, rowIndex, extraData)}
        </Col>
      </Row>
      <Row className="show-grid">
        <Col xs={12}>
          <div style={{ color: 'gray', fontStyle: 'italic' }}>{investigation.summary}</div>
        </Col>
      </Row>
      <Row className="show-grid" style={{ fontSize: '10px' }}>
        <Col xs={12} >
          <span >{extraData.doiFormatter(investigation.doi, investigation, rowIndex, extraData)}</span>
        </Col>
      </Row>
    </Grid>;

  }

  /**
   * This looks into the parameters of the investigation
   */
  getParameter(investigation, parameterName) {
    var parameter = _.find(investigation.parameters, function (o) {
      return o.name === parameterName;
    });
    if (parameter) {
      if (parameter.value) {
        return parameter.value;
      }
    }
    return;
  }

  sampleCountFormatter(cell, investigation, extraData) {
    var sampleCount = extraData.getParameter(investigation, SAMPLE_COUNT);
    if (sampleCount) {
      if (sampleCount !== 0) {
        return sampleCount;
      }
    }
  }

  fileCountFormatter(cell, investigation, extraData) {
    var fileCount = extraData.getParameter(investigation, FILE_COUNT);
    if (fileCount) {
      if (fileCount !== 0) {
        return (
          <span style={{ width: "50px", textAlign: "right", float: "left" }}>
            {fileCount}
          </span>
        );
      }
    }
  }

  datasetCountFormatter(cell, investigation, rowIndex, extraData) {
    var datasetCount = _.find(investigation.parameters, function (o) {
      return o.name === DATASET_COUNT;
    });
    if (datasetCount) {
      if (datasetCount.value) {
        if (datasetCount.value !== 0) {
          return (
            <div>
              <span
                style={{ width: "40px", textAlign: "right", float: "left" }}
              >
                {datasetCount.value}
              </span>
              <span
                style={{
                  width: "80px",
                  marginLeft: "5px",
                  float: "left",
                  fontStyle: "italic",
                  color: "#999999"
                }}
              >
                ({extraData.volumeFormatter(cell, investigation, extraData)})
              </span>
            </div>
          );
        }
      }
    }
  }

  /**
   * It return true if releaseDate < now
   */
  isReleased(investigation) {
    if (investigation) {
      if (investigation.releaseDate) {
        return true;
      }
    }
    return false;
  }


  componentDidMount() {
  }

  getColumns() {

    return [
      {
        text: "id",
        dataField: "id",
        hidden: true
      },
      {
        text: "Experiment",
        dataField: "name",
        formatter: this.experimentFormatter,
        formatExtraData: this,
        sort: true,
        hidden: false,
        headerStyle: (column, colIndex) => {
          return { width: "50%", textAlign: "center" };
        },
        responsiveHeaderStyle: {
          xs: { width: "100%" },
          sm: { width: "100%" },
          md: { hidden: true },
          lg: { hidden: true },
        }
      },
      {
        text: "Proposal",
        dataField: "name",
        formatter: this.nameFormatter,
        formatExtraData: this,
        sort: true,
        hidden: false,
        headerStyle: (column, colIndex) => {
          return { width: "50%", textAlign: "center" };
        },
        responsiveHeaderStyle: {
          xs: { hidden: true },
          sm: { hidden: true },
          md: { width: "140px", textAlign: "center" },
          lg: { width: "140px", textAlign: "center" }
        }
      },
      {
        text: "Beamline",
        dataField: "visitId",
        formatter: this.beamlineFormatter,
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden: true },
          sm: { hidden: true },
          md: { width: "140px", textAlign: "center" },
          lg: { width: "140px", textAlign: "center" }
        }
      },
      {
        text: "Title",
        dataField: "summary",
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden: true },
          sm: { hidden: true },

        }
      },
      {
        text: "Datasets",
        dataField: "datasets",
        formatter: this.datasetCountFormatter,
        formatExtraData: this,
        responsiveHeaderStyle: {
          xs: { hidden: true },
          sm: { hidden: true },
          md: { hidden: true },
          lg: { hidden: !this.props.showInvestigationStats, width: "150px" }
        }
      },
      {
        text: "Release",
        dataField: "releaseDate",
        formatter: this.dateFormatter,
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden: true },
          sm: { hidden: true },
          md: { hidden: true },
          lg: { width: "110px", textAlign: "center" }
        }
      },
      {
        text: "DOI",
        dataField: "doi",
        formatter: this.doiFormatter,
        sort: true,
        responsiveHeaderStyle: {
          xs: { hidden: true },
          sm: { hidden: true },
          md: { hidden: true },
          lg: { width: "300px", textAlign: "center" }
        }
      }
    ];
  }

  render() {
   
   
    const expandRow = {
      showExpandColumn: true,
      //expandByColumnOnly: true,
      expanded: this.props.expanded,
      expandColumnPosition: 'right',
      expandHeaderColumnRenderer: ({ isAnyExpands }) => {
        if (isAnyExpands) {
          return <span style={{ fontSize: '18px' }}><Glyphicon glyph="zoom-out" /></span>;
        }
        return <span style={{ fontSize: '18px' }}><Glyphicon glyph="zoom-in" /></span>;
      },
      expandColumnRenderer: ({ expanded }) => {
        if (expanded) {
          return (
            <Glyphicon glyph="zoom-out" />
          );
        }
        return (
          <Glyphicon glyph="zoom-in" />
        );
      },
      renderer: investigation => {        
        return (
          <InvestigationWidget
            investigation={investigation}
            sessionId={this.props.user.sessionId} >
          </InvestigationWidget>
        );
      }
    };


    if (this.props.fetching) {
      return <Loading message="Loading investigations" />;
    }

    return (
      <ResponsiveTable
        keyField="id"
        data={this.props.investigations}
        columns={this.getColumns()}
        expandRow={expandRow}
       
      />
    );
  }
}

export default InvestigationTable;
