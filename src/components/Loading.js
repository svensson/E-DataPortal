import React from 'react';
import './Loading.css';
import logo from '../images/loader.gif';

class Loading extends React.Component {
    render() {
        let message = "Loading...";
        if (this.props.message) {
            message = this.props.message;
        }
        return <div style={{ width: 300, height: 300, marginTop: 100 }} className="center-div">
            <div style={{ height: 150 }}><img style={{ marginTop: 100, height: 50, width: 50 }} alt="" src={logo} /></div>
            <div style={{ height: 100 }}><span style={{ marginTop: 200 }}>{message}</span></div>                    
                </ div>;   
}
}
export default Loading;
