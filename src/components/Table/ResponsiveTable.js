import React from 'react';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import BootstrapTable2 from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css';
import { getContainerSize } from "../../helpers/ResponsivenessHelper.js";

class InvestigationTable extends React.Component {
  
/**
 * This method will calculate the headerStyle based on the ResposiveHeaderStyle and the size of the window
 * @param {array} columns BootstrapTable2 columns 
 */
configure(columns){
    var size =  getContainerSize(window);     
    for (var i = 0; i < columns.length; i++){
        var column = columns[i];
        
        if (column.responsiveHeaderStyle){            
            if (column.responsiveHeaderStyle[size]){
                column.headerStyle = (column, colIndex) => {
                    return column.responsiveHeaderStyle[size];
                };
                if (column.responsiveHeaderStyle[size].hidden){
                   column.hidden = column.responsiveHeaderStyle[size].hidden;                   
                }
            }
        }    
    }
    return columns;
}
/**
 * This depends also in the size of the screen
 */
getSearchBarStyle(){
    var size =  getContainerSize(window);  
    switch(size) {
        case "xs":
            return {width:'100%', float:'center'};           
        case "sm":
            return {width:'100%', float:'center'};           
        case "md":
            return {width:'300', float:'right'};             
        case "lg":
            return {width:'500px', float:'right'};           
        default:
            return {width:'100%', float:'center'};
}
}
render() {    
    const { SearchBar } = Search;
  
    return <ToolkitProvider
            keyField="id"
            data={this.props.data }   
            columns={this.configure(this.props.columns)} 
            search           
            expandRow={this.props.expandRow}
            >
            {
                props => (
                    <div>
                        <div style={this.getSearchBarStyle()}>
                            <SearchBar { ...props.searchProps } />
                            <br/>
                        </div>
            <BootstrapTable2 selectRow={ this.props.selectRow } expandRow={this.props.expandRow}  { ...props.baseProps } pagination={ paginationFactory(this.props.pageOptions) }/>
                    </div>
                )
            }
            </ToolkitProvider>;
    }    

}

export default InvestigationTable;