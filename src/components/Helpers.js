/**
 * Returns a friendly readable string with the size
 * 
 * @param {integer} size Size in bytes
 * 
 */
  export function stringifyBytesSize(size) { 
     if (size > (1024 * 1024 * 1024 *1024)) {
      return  Math.trunc(size / (1024 * 1024 * 1024 * 1024))  + " TB";           
    }   
    if (size > (1024 * 1024 * 1024)) {
      return  Math.trunc(size / (1024 * 1024 * 1024))  + " GB";           
    }
    if (size > (1024 * 1024)) {      
      return Math.trunc(size / (1024 * 1024)) + " MB";
    }
    if (size > (1024)) {      
      return Math.trunc(size / (1024)) + " KB";
    }
    return size + " Bytes";
  }
