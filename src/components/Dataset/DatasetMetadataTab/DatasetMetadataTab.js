import React from 'react';
import ResponsiveTable from "../../Table/ResponsiveTable.js";
import './DatasetMetadataTab.css';

class DatasetMetadataTab extends React.Component {

  fieldFormatter(cell, row){
     return row.name + " : " + row.value;
  }

  getColumns(){
      return [
      {
        text: "id",
        dataField: "id",
        hidden: true
      },
      {
        text: "Field",
        dataField: "name",
        formatter: this.fieldFormatter,  
        responsiveHeaderStyle: {
          xs:  { hidden : false},
          sm:  { hidden : false},
          md:  { hidden : true },
          lg:  { hidden : true },
        }
        
      },
       {
        text: "Name",
        dataField: "name",
        responsiveHeaderStyle: {
          xs:  { hidden : true},
          sm:  { hidden : true},
          md:  { hidden : false },
          lg:  { hidden : false },
        }
        
      },
       {
        text: "Value",
        dataField: "value",
        responsiveHeaderStyle: {
          xs:  { hidden : true},
          sm:  { hidden : true},
          md:  { hidden : false },
          lg:  { hidden : false },
        }
        
      }];
      
  }

  render() {       
    /** Data sorted by name */
    let data = this.props.dataset.parameters.sort((a, b) => a.name.localeCompare(b.name, undefined, {sensitivity: 'base'}));  
       return  <div style={{ 'margin': '10px', fontSize:'12px'}}>
        <ResponsiveTable        
          keyField="id"
          data={ data }
          columns={ this.getColumns() }         
      /></div>;              
  }
}

export default DatasetMetadataTab;