import React, { Component } from 'react'
import { Image } from 'react-bootstrap';
import { getFileByEventId } from '../../api/icat/icatPlus.js';
import PropTypes from 'prop-types';
import ImageZoom from 'react-medium-image-zoom';

export default class GalleryDatasetThumbnail extends Component {
    render() {
        var gallery = this.props.dataset.parameters.filter(function (o) { return o.name === "ResourcesGallery" });
        var name = this.props.dataset.parameters.filter(function (o) { return o.name === "name" });

        if (gallery != null) {
            if (gallery.length > 0) {
                gallery = gallery[0].value.split(" ");
                if (gallery.length > this.props.index) {                    
                    var url = getFileByEventId(this.props.sessionId, this.props.dataset.investigation.id, gallery[this.props.index] );
                    return <ImageZoom image={{
                        src: url,
                        //alt: "test",
                        className: 'thumbnail',
                        style: { width: '100%' }
                      }} />;                    
                }
            }
        }
        return null;
    }
}

GalleryDatasetThumbnail.propTypes = {
    sessionId : PropTypes.string.isRequired,
    index : PropTypes.string.isRequired,
    dataset : PropTypes.object.isRequired    

}

