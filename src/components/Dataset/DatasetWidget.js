import React from 'react';
import { Badge, Tab, Tabs, Panel } from 'react-bootstrap';
import './DatasetTable.css';
import DatasetFooter from './DatasetFooter/DatasetFooter.js';
import DatasetSummary from './DatasetSummary/DatasetSummary.js';
import PublicationSummary from './PublicationSummary/PublicationSummary.js';
import DatasetDOIList from './DatasetDOIList/DatasetDOIList.js';
import DatasetMetadataTab from './DatasetMetadataTab/DatasetMetadataTab.js';
import DatasetTechniqueTab from './DatasetTechniqueTab/DatasetTechniqueTab.js';
import TECHNIQUES from '../../config/techniques/techniques.js';
import DatasetFileTree from '../../components/File/DatasetFileTree.js';
import _ from 'lodash';
import { FILE_COUNT } from '../../constants/ParameterTypes.js';
import InstrumentWidget from '../Instrument/InstrumentWidget.js';
import { getDatasetParameterByPrefixName } from '../../helpers/DatasetHelper.js';

class DatasetWidget extends React.Component {
    getDefinition(dataset){
        return  dataset.parameters.filter(parameter => parameter.name === "definition");
    }
   


    render() {
        let dataset = this.props.dataset;
        let definition = this.getDefinition(dataset);
        
        function getFilesTabTitle(dataset) {
            var fileCount = _.find(dataset.parameters, function (o) { return o.name === FILE_COUNT; });
            if (fileCount) {
                if (fileCount.value) {
                    return <div>Files <Badge style={{ backgroundColor: '#bfbfbf' }}>{fileCount.value}</Badge></div>;
                }
            }
            return "Files";

        }

        /** Case of publication */        
        if (definition){
            if (definition.length > 0){
                if (definition[0].value === "Publication"){
                    return  <PublicationSummary dataset={dataset} sessionId={this.props.sessionId} ></PublicationSummary>;
                }
            }            
        }

        return (<div style={{fontSize:'12px'}}><Panel>
            <Panel.Body >
                <Tabs id="tabs">
                    <Tab eventKey={1} title="Summary">
                        <DatasetSummary dataset={dataset} sessionId={this.props.sessionId} ></DatasetSummary>
                    </Tab>
                    {TECHNIQUES.map(function (technique, i) {
                        var params = dataset.parameters.filter(parameter => parameter.name.startsWith(technique.shortname));                        
                        if (params.length > 0) {
                            return <Tab eventKey={i + 2} title={technique.name}>
                                <DatasetTechniqueTab dataset={dataset} params={params}></DatasetTechniqueTab>
                            </Tab>
                        }
                        return null;
                    })}
                    {!getDatasetParameterByPrefixName(this.props.dataset, "Instrument").length == 0 &&
                        <Tab eventKey={9} title="Instrument" >
                            <InstrumentWidget dataset={dataset} ></InstrumentWidget>
                        </Tab>
                    }

                    
                    <Tab eventKey={11} title={getFilesTabTitle(dataset)} mountOnEnter={true}>
                        <DatasetFileTree dataset={dataset} sessionId={this.props.sessionId}> ></DatasetFileTree>
                    </Tab>
                    <Tab eventKey={10} title="Metadata List" className="pull-right" >
                        <DatasetMetadataTab dataset={dataset} ></DatasetMetadataTab>
                    </Tab>
                    
                </Tabs>
            </Panel.Body>
            <Panel.Footer>
                <DatasetFooter location={this.props.dataset.location} sessionId={this.props.sessionId} dataset={this.props.dataset} ></DatasetFooter>
            </Panel.Footer>
        </Panel><br /><br /><br /><br /></div>


        );

    }

/*
                    <Tab eventKey={12} title="DOI" mountOnEnter={true}>
                        <DatasetDOIList dataset={dataset} sessionId={this.props.sessionId}> ></DatasetDOIList>
                    </Tab>
*/

}

export default DatasetWidget;


