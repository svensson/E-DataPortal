import React from 'react';
import Moment from 'react-moment';
import { Grid, Row, Col, Image } from 'react-bootstrap';
import './DatasetSummary.css';
import GalleryDatasetThumbnail from '../GalleryDatasetThumbnail.js'
import { getDatasetParameterValueByName } from '../../../helpers/DatasetHelper.js';
import ParameterTableWidget from '../../Instrument/ParameterTableWidget.js';

class MXDatasetSummary extends React.Component { 
   
    getParameters(){
        return [
            {name : 'Name', value : this.props.dataset.name},
            {name : 'Definition', value : getDatasetParameterValueByName(this.props.dataset, "definition")},
            {name : 'Start', value : <Moment parse="YYYY-MM-DD HH:mm" format="LTS">{this.props.dataset.startDate}</Moment>},
            {name : 'Sample', value : this.props.dataset.sampleName},
            {name : 'Images', value : getDatasetParameterValueByName(this.props.dataset, "MX_numberOfImages")},
            {name : 'Transmission', value : getDatasetParameterValueByName(this.props.dataset, "MX_transmission") + " %"},
            {name : 'Prefix', value : getDatasetParameterValueByName(this.props.dataset, "MX_template")}
            
        ];
    }
    toExponential(value){
        if (value){
            try{
                return parseFloat(value).toExponential();
            }
            catch(e){}
        }
        return "";
    }
    getTechniqueParameters(){
        return [
            
            {name : 'Resolution', value : getDatasetParameterValueByName(this.props.dataset, "MX_resolution") + " Å" },
            {name : 'Wavelength', value : getDatasetParameterValueByName(this.props.dataset, "InstrumentMonochromator_wavelength") + " Å" },
            {name : 'Exposure Time', value : getDatasetParameterValueByName(this.props.dataset, "MX_exposureTime") + " s"},           
            {name : 'Flux start', value : this.toExponential(getDatasetParameterValueByName(this.props.dataset, "MX_flux"))},
            {name : 'Flux end', value : this.toExponential(getDatasetParameterValueByName(this.props.dataset, "MX_fluxEnd"))},
            {name : 'X Beam', value : getDatasetParameterValueByName(this.props.dataset, "MX_xBeam") + " mm"},
            {name : 'Y Beam', value : getDatasetParameterValueByName(this.props.dataset, "MX_yBeam") + " mm"}
            
        ];
    }

    render() {
        
        return (
            <Grid fluid style={{margin:'20px'}}>
                <Row>
                    <Col xs={12} md={2}>                       
                        <ParameterTableWidget striped={false} parameters={this.getParameters()} ></ParameterTableWidget>   

                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <ParameterTableWidget striped={false} parameters={this.getTechniqueParameters()} ></ParameterTableWidget> 
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={0} ></GalleryDatasetThumbnail>
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={1} ></GalleryDatasetThumbnail>
                    </Col>
                  
                    <Col xs={12} sm={12} md={2}>
                         <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={2} ></GalleryDatasetThumbnail>
                    </Col>
                </Row>
            </Grid>


        );
    }
}

export default MXDatasetSummary;