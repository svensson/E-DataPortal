import React from 'react';
import Moment from 'react-moment';
import { Grid, Row, Col, Image } from 'react-bootstrap';
import './DatasetSummary.css';
import EMDatasetSummary from './EMDatasetSummary';
import MXDatasetSummary from './MXDatasetSummary';

import GalleryDatasetThumbnail from '../GalleryDatasetThumbnail.js' 
import { getDatasetParameterValueByName, getDatasetParameterByPrefixName } from '../../../helpers/DatasetHelper.js';
import ParameterTableWidget from '../../Instrument/ParameterTableWidget.js';

class DatasetSummary extends React.Component {

    getParameters(){
        return [
            {name : 'Name', value : this.props.dataset.name},
            {name : 'Definition', value : getDatasetParameterValueByName(this.props.dataset, "definition")},
            {name : 'Start', value : <Moment parse="YYYY-MM-DD HH:mm" format="LTS">{this.props.dataset.startDate}</Moment>},
            {name : 'Sample', value : this.props.dataset.sampleName},
            {name : 'Description', value : getDatasetParameterValueByName(this.props.dataset, "Sample_description")}
        ];
    }
      render() {
        var technique = getDatasetParameterValueByName(this.props.dataset, "definition");
        if (technique){
            if (technique === 'EM'){
                return <EMDatasetSummary dataset={this.props.dataset}></EMDatasetSummary>
            }
        }
        else{
            /** some MX datasets has no definition but we can figure out that it is MX because will have many MX_ parameters */
            if (getDatasetParameterByPrefixName(this.props.dataset, "MX_").length > 1){
                /** It is MX */
                return <MXDatasetSummary dataset={this.props.dataset}></MXDatasetSummary>
            }
        }
        
        return (
            <Grid fluid style={{margin:'20px'}}>
                <Row>
                    <Col xs={12} md={3}>                       
                        <ParameterTableWidget striped={true} parameters={this.getParameters()} ></ParameterTableWidget>   
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={0} ></GalleryDatasetThumbnail>                        
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={1} ></GalleryDatasetThumbnail>                        
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={2} ></GalleryDatasetThumbnail>                        
                    </Col>
                    <Col xs={12} sm={12} md={2}>
                        <GalleryDatasetThumbnail dataset={this.props.dataset} sessionId={this.props.sessionId} index={3} ></GalleryDatasetThumbnail>                        
                    </Col>                   
                </Row>
            </Grid>


        );
    }
}

export default DatasetSummary;