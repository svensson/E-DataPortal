import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './DatasetTechniqueTab.css';

class DatasetTechniqueTab extends React.Component {

  componentDidMount(){    
  }

  render() {     
    const options = {
      paginationSize: 10,
      sizePerPage: 20,
      paginationShowsTotal: true,
      hidePageListOnlyOnePage: true
    };
  
    var keys = this.props.params.sort((a, b) => a.name.localeCompare(b.name, undefined, {sensitivity: 'base'}));  
    var data = [];
    for (var index in keys){            
        data.push({
          key : keys[index].name,
          value : keys[index].value
        })
    } 
    return (
      <div style={{padding:10}}>
      <BootstrapTable
            data={ data } 
            options={ options }
            pagination
            striped
            search          
            hover
            condensed>                        
            <TableHeaderColumn width='20%'  isKey dataField='key'>Name</TableHeaderColumn>
            <TableHeaderColumn dataSort width='80%' dataField='value'>Value</TableHeaderColumn>          
          </BootstrapTable>          
                     
          </div>
    );
  }
}

export default DatasetTechniqueTab;