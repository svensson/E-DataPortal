import React from 'react';
import { Grid, Row, Col, Image } from 'react-bootstrap';
import './PublicationSummary.css';
import { getFileByEventId } from '../../../api/icat/icatPlus.js';

class PublicationSummary extends React.Component {
    getImageFromGalleyByIndex(index) {
        /** Checking gallery **/
        var gallery = this.props.dataset.parameters.filter(function (o) { return o.name === "ResourcesGallery" });

        if (gallery != null) {
            if (gallery.length > 0) {
                gallery = gallery[0].value.split(" ");
                if (gallery.length > index) {                    
                    var url = getFileByEventId(this.props.sessionId, this.props.dataset.investigation.id, gallery[index] );
                    return <div><br /><Image src={url} height={200}  /></div>;                    
                }
            }
        }
        return null;
    }

    getParameterValue(name){
        var sampleNameParameter = this.props.dataset.parameters.filter(function (o) { return o.name === name });
        if (sampleNameParameter){
            if (sampleNameParameter[0]){
                return sampleNameParameter[0].value;
            }
        }
    }

    render() {
        
        return (
            <Grid fluid>
                <Row>
                    <Col xs={12} sm={12} md={2}>
                        {this.getImageFromGalleyByIndex(0)}
                    </Col>
                    <Col xs={12} md={8}>  
                        <div style={{ fontSize: '10px'}}>                     
                            {this.getParameterValue("Sample_description")}
                       </div>
                    </Col>                   
                </Row>
            </Grid>


        );
    }
}

export default PublicationSummary;