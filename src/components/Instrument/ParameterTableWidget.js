import React from 'react';
import './ParameterTableWidget.css';
import { Table } from 'react-bootstrap';
import _ from 'lodash';

class ParameterTableWidget extends React.Component {
  getColumnNameClass(value){ 
      if ((!value)|| (value === "")||(value === null)){          
          return "gray-out";
      }       
  }  
  getColumnValueClass(value){ 
    if ((!value)|| (value === "")||(value === null)){          
        return "gray-out";
    }       
    return "value";
}  
  getRow(name, value){ 
      return <tr ><td className={this.getColumnNameClass(value)}>{name}</td><td className={this.getColumnValueClass(value)}>{value}</td></tr>
  }
  getColumns(names, values){      
      return  <tbody>  
                 {names.map((name, i) => this.getRow(names[i], values[i]))}            
            </tbody>;
  }  

  getHeader(name){
      if (name){
            return  <thead>
                <tr>
                    <th colSpan="2">{name}</th>					
                </tr>
            </thead>;
      }
  }  

  render(){    
      var parameterNames = [];  
      var parameterValues = [];  
      if ((this.props.names) &&(this.props.values)){                 
            parameterNames = this.props.names.trim().split(" ");
            parameterValues = this.props.values.trim().split(" ");                         
      }

      if (this.props.parameters){           
          for (var i in this.props.parameters){
              parameterNames.push(this.props.parameters[i].name);
              parameterValues.push(this.props.parameters[i].value);
          }
      }

      /** If all values are empty then it is not displayed */
      //if (!_.find(parameterValues, function(value){ return value !== null})){
      //    return null;
     // }
     var striped = true;
     if (this.props.striped == false){
         striped = this.props.striped;
     }
      
      return  <Table  striped={striped} condensed hover> 
                        {this.getHeader(this.props.header)}                              
                        {this.getColumns(parameterNames, parameterValues)}                                                                                       
      </Table>;
  
  }
};

export default ParameterTableWidget;