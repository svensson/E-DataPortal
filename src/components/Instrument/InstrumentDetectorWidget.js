import React from 'react';
import { getDatasetParameterByName, getDatasetParameterByPrefixName } from '../../helpers/DatasetHelper.js';
import ParameterTableWidget from './ParameterTableWidget.js';

class InstrumentDetectorWidget extends React.Component {
 

  getDetectorWidget(index){  
      var parameters = getDatasetParameterByPrefixName(this.props.dataset, "InstrumentDetector0" + index);    
      if (parameters){
          if (parameters.length > 0){
             return <DetectorWidget              
                detectorIndex={index}
                name = {getDatasetParameterByName(this.props.dataset, "InstrumentDetector0" + index + "_name")}
                positionersValue = {getDatasetParameterByName(this.props.dataset, "InstrumentDetector0" + index + "Positioners_value")}
                positionersName = {getDatasetParameterByName(this.props.dataset, "InstrumentDetector0" + index + "Positioners_name")}
                positionersRoisValue = {getDatasetParameterByName(this.props.dataset, "InstrumentDetector0" + index + "PositionersRois_value")}
                positionersRoisName = {getDatasetParameterByName(this.props.dataset, "InstrumentDetector0" + index + "PositionersRois_name")}             
                ></DetectorWidget>;
          }
      }     
  }

  render(){      
      var detectors = [];
      for(var i = 1; i < 10; i++){
          var detector = this.getDetectorWidget(i);        
          if (detector){
            detectors.push(detector);
          }
      }     
     if (detectors.length == 0){
         return null;
     }
    return <div className="container-fluid">                
            <h3>Detectors</h3>
                <div className="row">
                    {detectors.map(function (detector, i) {                    
                        return   <div>{detector}</div>
                    })}
                </div>                                        	           
            </div>;
      
  }
}

export default InstrumentDetectorWidget;

class DetectorWidget extends React.Component {
    
  render(){
      return  <div className="col-sm-1">                            
                    <ParameterTableWidget header={"#" + this.props.detectorIndex + this.props.name} names={this.props.positionersName}  values={this.props.positionersValue} ></ParameterTableWidget>	                                                           
	            </div>;
  }
};



