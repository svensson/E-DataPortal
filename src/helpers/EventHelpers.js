import React from 'react'
import Moment from 'moment'
import _ from 'lodash'
import { Glyphicon } from 'react-bootstrap'
import { ANNOTATION, NOTIFICATION } from '../constants/EventTypes';

//sets global constants
export const COLLAPSED_MODE = "collapsedState"
export const EXPANDED_VIEW_MODE = "expandedViewingState"
export const EDITION_MODE = "expandedEditingState"
export const HISTORY_MODE = "viewingHistoryState"

/**
 * Get the visual representation of the event's category. Ban cicle Glyph provided when there is no category
 * @param {*} category the event category
 * @param {string} size the size in px of the icon, 25 by default
 */
export function getEventIcon(category, size) {
    let iconSize;
    if (size) {
        iconSize = size + "px";
    } else {
        iconSize = "25px";
    }
    if (category) {
        if (category === 'comment') {
            return (<img src='/comment.png' width={iconSize} alt='comment.png' />)
        } else if (category === 'commandLine') {
            return <Glyphicon glyph='expand' style={{ fontSize: iconSize, position: 'static' }} />
        } else if (category === 'error') {
            return <Glyphicon glyph='alert' style={{ fontSize: iconSize, color: '#d03436', position: 'static' }} />
        } else if (category === 'info') {
            return <Glyphicon glyph='info-sign' style={{ fontSize: iconSize, color: '#3366CC', position: 'static' }} />
        } else if (category === 'debug') {
            return <Glyphicon glyph='exclamation-sign' style={{ fontSize: iconSize, color: 'gray', position: 'static' }} />
        } else if (category === 'previousVersion') {
            return (<img src='/previousVersion.png' width={iconSize} alt='previousVersion.png' />)
        }
    }
    // no category was found
    return <Glyphicon glyph='ban-circle' style={{ fontSize: iconSize }} />
}

/**
 * A helper function which returns the formatted date the event was created
 * @param {event} event the investigated event
 * @returns {string} the date in the format HH:mm 
*/
export function getEventCreationDate(event) {
    return (Moment(event.creationDate).format("HH:mm:ss"))
}

/**
 * Get the formatted event creation date for an event history
 * @param {event} event the event to retrieve the creation date from
 * @returns {String} the formatted date under the from  MMM DD YYYY HH:mm a
 */
export function getEventHistoryCreationDate(event) {
    return Moment(event.creationDate).format("MMM DD YYYY HH:mm:ss")
}

/**
 * Get the original event which the provided event has modified. This original event is the more nested event inside the provided event.
 * @param {event} event the event which has modified the original event.
 * @return {event} the original event. If there is no original event (ie the provided event is the original event) returns itself.
 */
export function getOriginalEvent(event) {
    let e = event;
    if (e) {
        while (e.previousVersionEvent) {
            e = e.previousVersionEvent;
        }
        return e;
    }
    return null;
}

/**
 * Get the original username for the given event.
 * @param {*} event the event under investigation
 * @returns {string} the original username, null if not found
 */
export function getOriginalUsername(event) {
    let originalEvent = getOriginalEvent(event);
    return originalEvent.username ? originalEvent.username : null;
}

/**
 * Get the number of previous version an given event has.
 * @param {event} event the event which is investigated
 * @returns {number} the number of previous event version which exists
 */
export function getPreviousVersionNumber(event) {
    let e = event;
    if (e) {
        let previousVersionNumber = 0;
        while (e.previousVersionEvent) {
            previousVersionNumber += 1;
            e = e.previousVersionEvent;
        }
        return previousVersionNumber;
    }
    return 0;
}

/**
 * Get the text from a given event content
 * @param {array} content 
 * @param {string} format the requested format ('plainText', 'html's )
 * @returns {string} the text of content. Null if not found.
 */
export function getContent(content, format) {
    if (content) {
        for (let item of content) {
            if (item.format) {
                if (item.format === format) {
                    if (item.text) {
                        return item.text;
                    }
                }
            }
        }
    }
    return null;
}

/**
 * Get the last comment of an event
 * @param {*} event 
 * @returns {array} the content of the comment in all different formats available
 */
export function getLastCommentContent(event) {
    if (event) {
        if (event.type) {
            if (event.type === ANNOTATION) {
                return event.content;
            } else if (event.type === NOTIFICATION) {
                let nbPreviousVersion = getPreviousVersionNumber(event);
                if (nbPreviousVersion > 0) {
                    return event.content
                }
            }
        }
    }
    return null;
}

/**
 * Images to thumbnails
 */
export function convertImagesToThumbnails(html) {
    if (html) {
        return html.replace(new RegExp('img src', 'g'), 'img style="height:200px;width:auto" src');
    }
};

/**
    * Get the first letters of the event content. 
    * @param {array} content the event content
    * @param {integer} nbCharacterToDisplay the number of characters to be returned
    * @returns {string} the portion of plain text composed of the nbCharacterToDisplay first characters
    */
export function getFirstLettersofContent(content, nbCharacterToDisplay) {
    let text = getContent(content, 'plainText');
    if (text) {
        if (nbCharacterToDisplay) {
            if (text.length > nbCharacterToDisplay) {
                return text.slice(0, nbCharacterToDisplay) + " ..."
            } else {
                return text.slice(0, nbCharacterToDisplay)
            }
        } else {
            return text.slice(0, 10) + " ..."
        }
    }

    return ''
}

/**
 * Get the tags of an event
 * @param {object} event the event to get the tag from
 * @returns {string} the list of tags
 */
export function getTag(event) {
    if (event) {
        if (event.tag && event.tag instanceof Array) {
            return _.join(event.tag, " ")
        }
    }
    return "";
}

/**
 * Get the corresponding tag objects for a given event
 * @param {object} event event to get the tag id from
 * @param {array} availables list of available tags
 * @returns {array} list of tag objects for the given event
 */
export function getTags(event, availableTags) {
    let eventTags = [];
    if (event && event.tag && event.tag.length !== 0) {
        event.tag.forEach(tagId => {
            let tag = _.find(availableTags, (availableTag) => _.isEqual(availableTag._id, tagId))
            if (tag) { eventTags.push(tag); };
        });
        return eventTags;
    }
    return null;
}

/**
 * HTTP error parser. THis is meant to handle HTTP error such as 400, 403, 404 creating a general error message for these types of errors.
 * @param {*} error HTTP error
 * @returns {String} Error message
 */
export function parseHTTPError(error) {
    if (error) {
        if (error.response) {
            if (error.response.status === 403) {
                return 'You do not have the permission to access this resource.'
            }
        }
    }
    return 'An error has occured.'
}