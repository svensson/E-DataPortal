import ALIAS from '../config/parameters/alias.js';

export function convertParameterNameToAlias(parameterName) {
    if (ALIAS[parameterName]){
        return ALIAS[parameterName];
    }
    return parameterName;
};